#pragma once
#include "math.h"
#include "Color.h"
#include "Texture.h"
#include "Shader.h"
#include "Array.h"

class Material
{
	template <typename T>
	struct Param
	{
		GLint location = -1;
		T value;
		char name[32];
		Param() = default;
		Param(GLint location, T val): location(location), value(val) { *name = 0; }
		Param(GLint location, T val,const char* newName): location(location), value(val) { strcpy_s(name,32,newName); }
		bool operator == (const char* str) const { return strcmp(name, str) == 0; };
		bool operator == (const Param<T>& o) const { return location == o.location; };
	};

	// Parameter types
	typedef Param<float> FP; // float
	typedef Param<Vec2> F2P; // float2
	typedef Param<Vec3> F3P; // float3
	typedef Param<Vec4> F4P; // float4
	typedef Param<Mat4> M4P; // Matrix4
	typedef Param<Texture*> TP; // Texture

	// Parameter values
#define MAX_ATTRIBUTES 16
	Array<FP, MAX_ATTRIBUTES> fParams;
	Array<F2P, MAX_ATTRIBUTES> f2Params;
	Array<F3P, MAX_ATTRIBUTES> f3Params;
	Array<F4P, MAX_ATTRIBUTES> f4Params;
	Array<M4P, MAX_ATTRIBUTES> m4Params;
	Array<TP, 8> tParams;

	Shader* shader = nullptr;

	static Material* boundMaterial;
public:
	enum BlendMode
	{
		None,
		Alpha,
		Multiply,
		Add,
		PremultipliedAlpha
	};

	Material() = default;
	~Material();

	void setShader(Shader* shader);
	Shader* getShader();

	void setBlendMode(BlendMode mode);
	void setBlendMode(const char* name);
	BlendMode getBlendMode()const;

	// Set Parameter
	void setFloat(const char* name, float value);
	void setVec2(const char* name, Vec2 value);
	void setVec3(const char* name, Vec3 value);
	void setVec4(const char* name, const Vec4& value);
	void setMat4(const char* name, const Mat4& value);
	void setTexture(const char* name, Texture* value);
	void setColor(const char* name, const Color value);

	bool trySetFloat(const char* name, float value);
	bool trySetVec2(const char* name, Vec2 value);
	bool trySetVec3(const char* name, Vec3 value);
	bool trySetVec4(const char* name, const Vec4& value);
	bool trySetMat4(const char* name, const Mat4& value);
	bool trySetTexture(const char* name, Texture* value);
	bool trySetColor(const char* name, const Color value);

	// Get Parameter
	float getFloat(const char* name) const;
	Vec2 getVec2(const char* name) const;
	Vec3 getVec3(const char* name) const;
	Vec4 getVec4(const char* name) const;
	Texture* getTexture(const char* name) const;
	Color getColor(const char* name) const;

	Texture* getMainTexture() const;

	void bind();
	static void unbind();
private:
	BlendMode blendMode = BlendMode::None;
};
