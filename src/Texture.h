#pragma once
#include "Image.h"
#include "platform.h"

class Texture : NonCopyable
{
public:
	enum FilteringMode
	{
		None,
		Smooth,
		Nearest,
		Bilinear,
		Trilinear,
	};

	Texture() = default;
	Texture(Image&);
	~Texture();

	void setFiltering(FilteringMode mode);
	void setFiltering(std::string mode);
	int getWidth() const ;
	int getHeight() const ;

	static void bind(const Texture& tex, int slot = 0);
	static void unbind(int slot = 0);
private:
	static GLuint boundTexture[8];

	GLuint texture = 0;
	int width = 0, height = 0;
	bool mipmaps = 0;
	FilteringMode filtering = FilteringMode::None;

	void genMipMaps();
};