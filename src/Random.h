#pragma once
#include "common.h"

/*
WELLRNG512 Random Number generator
*/
class Random
{
	/* initialize state to random bits */
	UInt32 state[16];
	/* init should also reset this to 0 */
	UInt32 index = 0;

public:
	Random();
	UInt32 nextUInt32();
	UInt64 nextUInt64();
	Int32 inRange(Int32 minVal, Int32 maxVal); // in range [minVal;maxVal)
};