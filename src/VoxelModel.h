#pragma once
#include "common.h"
#include "Color.h"
#include "math.h"
#include "Renderer.h"
#include "Array.h"
#include "VoxelData.h"
#include <vector>

class Image;
class VoxelModel
{
	std::vector<VoxelData> model[16];
	std::vector<VoxelPoint> visible[16];
	Array<Vec3i, 16> size;
	bool lodValid[16];
	bool visibleValid[16];


	void generateLod(unsigned lod);
public:
	VoxelModel();
	VoxelModel(Vec3i size, std::vector<VoxelData>);

	void create(Vec3i, Color color = Color::White);
	void createAndCarve(const Image* front, const Image* left, const Image* top, Color color = Color::White);
	void carve(const Image* front, const Image* left, const Image* top);
	void paint(const Image* front, const Image* left, const Image* top);
	std::vector<VoxelPoint>& getVisible(unsigned lod = 0);
	void invalideLods();
	void genLods(int lod);
	unsigned getMaxLod();
	Vec3i getSize(unsigned lod = 0);
	std::vector<VoxelData>& getModel(int lod = 0);

	void destoySphere(Vec3i pos, int r);
	std::vector<VoxelPoint> destoyAndGetSphere(Vec3i pos, int r);


	inline int arrayPos(int x, int y, int z, unsigned lod = 0) const;
	inline int arrayPos(int x, int y, int z, Vec3i size) const;

};