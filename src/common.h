#pragma once
#include <cstdint>
#include <string>

// Macros:
#define DEBUG

// Basic Types
typedef std::int_fast32_t Int;
typedef std::uint_fast32_t UInt;

typedef std::int8_t Int8;
typedef std::uint8_t UInt8;
typedef std::int16_t Int16;
typedef std::uint16_t UInt16;
typedef std::int32_t Int32;
typedef std::uint32_t UInt32;
typedef std::int64_t Int64;
typedef std::uint64_t UInt64;

typedef float Float;
typedef double Double;

// Game Types
struct Entity
{
	typedef UInt32 Idx;
	typedef UInt32 Version;
	Idx idx;
	Version version;
	inline Entity(Idx idx = 0, Version version = 0): idx(idx), version(version) {}
	inline bool operator < (Entity rhs) const  { return *reinterpret_cast<const UInt64*>(this) < *reinterpret_cast<UInt64*>(&rhs); }
	inline bool operator == (Entity rhs) const { return *reinterpret_cast<const UInt64*>(this) == *reinterpret_cast<UInt64*>(&rhs); }
};
typedef UInt32 Component;
typedef UInt32 SystemId;

// Constants
const Int MAX_ENTITY_NAME_LENGTH = 32;
const Int MAX_ENTITIES = 8092;

// Useful
class NonCopyable
{
protected:
	NonCopyable() {}
	~NonCopyable() {} /// Protected non-virtual destructor
private:
	NonCopyable(const NonCopyable &);
	NonCopyable & operator = (const NonCopyable &);
};