#pragma once 
#include "common.h"
#include "System.h"
#include "Array.h"
#include "math.h"
class TransformStorage : public System
{
	Transform transforms[MAX_ENTITIES];
public:
	inline TransformStorage(SystemId id):System(id) {}
	inline Transform& getTransform(Entity e) { return  transforms[e.idx]; }
	virtual void onEntityDestroy(Entity e) override final { }
};