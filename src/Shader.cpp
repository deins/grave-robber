#include "Shader.h"
#include "debug.h"
#include <fstream>
#include <memory>
#include <algorithm>
#include <cstring>

using namespace std;

ShaderStage::~ShaderStage()
{
	if (shader > 0)
	{
		glDeleteShader(shader);
		glDebugCheck();
	}
}

void ShaderStage::createFromSource(const char* source, ShaderStage::Type type)
{
	assert(this->type == Undefined && type != Undefined);
	this->type = type;
	// Create the shader object 
	shader = glCreateShader(type);
	if (shader == 0)
		fatalError("glCreateShader() failed!");

	// Upload shader
	glShaderSource(shader, 1, &source, nullptr);

	// Compile shader
	GLint status = 0;
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{// Log error
		GLint infoLen = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
		if (infoLen > 1)
		{
			unique_ptr<char> infoLog(new char[sizeof(GLchar) * infoLen]);
			glGetShaderInfoLog(shader, infoLen, NULL, infoLog.get());
			fatalError("Compilation failed:\n%s\n", infoLog.get());
		}
		else fatalError("Error compiling shader! Reason unknown.\n");
	}
	glDebugCheck();
}

void ShaderStage::loadFromFile(const char* filename)
{
	log("Loading shader %s", filename);
	const char* format = strrchr(filename, '.');
	if (format == nullptr) fatalError("shader: unknown file format! can't decide shader type!");
	++format;
	Type type = Undefined;
	if (strcmp(format, "vert") == 0)
	{
		type = Vertex;
	}
	else if (strcmp(format, "frag") == 0)
	{
		type = Fragment;
	}
	else if (strcmp(format, "geom") == 0)
	{
		type = Geometry;
	}
	else
	{
		fatalError("shader: unknown file format '%s'! can't decide shader type!", format);
	}
	auto fileContent = platform::readAssetFile(filename);
	assert(fileContent.size() > 0);
	createFromSource(reinterpret_cast<const char*>(fileContent.data()), type);
}

const char* ShaderStage::getTypeName(Type t) const
{
	switch (t)
	{
		case(Undefined) :
			return "Undefined";
		case(Vertex) :
			return "VoxelPoint";
		case(Fragment) :
			return "Fragment";
		case(Geometry) :
			return "Geometry";
		case(Compute) :
			return "Compute";
		case(TesselationControl) :
			return "TesselationControl";
		case(TesselationEvaluation) :
			return ("TesselatiTesselationEvaluation");
		default:
			assert(false);
			return "ShaderStage::Invalid";
	}
}

GLuint Shader::boundShaderProgram = 0;


Shader::~Shader()
{
	if (shaderProgram > 0)
	{
		glDeleteProgram(shaderProgram);
		log("~shader() %d", shaderProgram);
	}
	glDebugCheck();
}

GLint  Shader::getUniformLocation(const std::string& name) const
{
	auto it = find(uniforms.begin(), uniforms.end(), name);
	if (it == uniforms.end())
	{
		return -1;
	}
	return it->location;
}

GLint Shader::getAttributeLocation(const std::string& name) const
{
	auto it = find(attributes.begin(), attributes.end(), name);
	if (it == attributes.end())
	{
		return -1;
	}
	return it->location;
}

void Shader::addStage(ShaderStage* stage)
{
	assert(!linked);
	if (shaderProgram == 0)
	{
		shaderProgram = glCreateProgram();
	}
	glAttachShader(shaderProgram, stage->shader);
}

void Shader::link()
{
	assert(shaderProgram != 0 && !linked);
	linked = true;

	{// Link
		glLinkProgram(shaderProgram);

		GLint status;
		glGetProgramiv(shaderProgram, GL_LINK_STATUS, &status);
		char infoLog[256];
		*infoLog = 0;
		glGetProgramInfoLog(shaderProgram, 256, NULL, infoLog);
		if (status == GL_FALSE)
			fatalError("shader linking failed: %s\n", infoLog);
		else
			log("Shader linked! status: %s", &infoLog);
	}

	// Get uniforms and attributes
	{
		GLint count;
		// Attributes			
		glGetProgramiv(shaderProgram, GL_ACTIVE_ATTRIBUTES, &count);
		attributes.resize(count);
		for (int i = 0; i < count; ++i)
		{
			Attribute& c = attributes[i];
			glGetActiveAttrib(shaderProgram, i, 32, NULL, &(c.size), &(c.type), c.name);
			c.location = glGetAttribLocation(shaderProgram, c.name);
			assert(c.location != -1);
		}
		attributes.erase(remove_if(attributes.begin(), attributes.end(), [](const Attribute& v) { return v.name[0] == '_'; }), attributes.end()); // remove driver defined attributes
		//sort(attributes.begin(), attributes.end(), [](const Attribute& a, const Attribute& b)->bool{return a.name < b.name;});
		// Uniforms
		glGetProgramiv(shaderProgram, GL_ACTIVE_UNIFORMS, &count);
		uniforms.resize(count);
		for (int i = 0; i < count; ++i)
		{
			Uniform& c = uniforms[i];
			glGetActiveUniform(shaderProgram, i, 32, NULL, &(c.size), &(c.type), c.name);
			c.location = glGetUniformLocation(shaderProgram, c.name);
			assert(c.location != -1);
		}
		uniforms.erase(remove_if(uniforms.begin(), uniforms.end(), [](const Uniform& v) { return v.name[0] == '_'; }), uniforms.end()); // remove driver defined uniforms
		//sort(uniforms.begin(), uniforms.end(), [](const Uniform& a, const Uniform& b)->bool{return a.name < b.name;});
	}
	glDebugCheck();
}

void Shader::bind()
{
	if (shaderProgram != boundShaderProgram)
	{
		assert(shaderProgram != 0 && linked);
		boundShaderProgram = shaderProgram;
		glUseProgram(shaderProgram);
		glDebugCheck();
	}
}

void Shader::unbind()
{
	boundShaderProgram = 0;
	glUseProgram(0);
	glDebugCheck();
}

bool Shader::Attribute::operator < (const std::string& str) const
{
	return name < str;
}

bool Shader::Uniform::operator < (const std::string& str) const
{
	return name < str;
}

bool Shader::Attribute::operator == (const std::string& str)const
{
	return name == str;
}

bool Shader::Uniform::operator == (const std::string& str) const
{
	return name == str;
}

const std::vector<Shader::Uniform>& Shader::getUniforms() const
{
	return uniforms;
}
const std::vector<Shader::Attribute>& Shader::getAttributes() const
{
	return attributes;
}
