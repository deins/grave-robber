#pragma once
#include "common.h"

class System : NonCopyable
{
protected:
	const SystemId systemId;
public:
	inline System(SystemId id):systemId(id) {}
	virtual ~System() = default;

	virtual void init() {}
	virtual void onEntityDestroy(Entity e) = 0;
};