#include "Image.h"
#include "debug.h"
#define STBI_ASSERT(x) assert(x)
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include "platform.h"

Image::~Image()
{
	if (pixels != nullptr) stbi_image_free(pixels);
}

void Image::loadFromFile(const char* filename, int requiredComponents)
{
	auto file = platform::readAssetFile(filename);
	pixels = stbi_load_from_memory(file.data(), (int)file.size(), &width, &height, &components, requiredComponents);
	if (pixels == nullptr || components == 0) fatalError("Can't read file %s!", filename);
	if (requiredComponents != 0 && requiredComponents != components) fatalError("Image wrong component count!");
}

int Image::getWidth() const
{
	return width;
}

int Image::getHeight() const
{
	return height;
}

int Image::getComponents() const
{
	return components;
}

Color Image::getPixel(int x, int y) const
{
	assert(width > 0 && height > 0);
	auto* ptr = (pixels + y*width*components + x*components);
	switch (components)
	{
		case (1) :
			// Grey
			return Color(*ptr);
			break;
		case(2) :
			// Grey + alpha
			return Color(*ptr, *ptr, *ptr, *(ptr + 1));
			break;
		case(3) :
			// RGB
			return Color(*(ptr++), *(ptr++), *(ptr));
			break;
		case(4) :
			// RGBA
			return *reinterpret_cast<Color*>(ptr);
			break;
		default:
			fatalError("Image: wrong number of components!");
			break;
	}
}

void Image::setPixel(int x, int y, Color c)
{
	assert(width > 0 && height > 0);
	auto* ptr = (pixels + y*width*components + x*components);
	switch (components)
	{
		case (1) :
			// Grey
			*ptr = c.getBrightness();
			break;
		case(2) :
			// Grey + alpha
			*(ptr++) = c.getBrightness();
			*ptr = c.a;
			break;
		case(3) :
			// RGB
			*(ptr++) = c.r;
			*(ptr++) = c.g;
			*(ptr) = c.b;
			break;
		case(4) :
			// RGBA
			*(reinterpret_cast<UInt32*>(ptr)) = reinterpret_cast<UInt32>(&c);
			break;
		default:
			fatalError("Image: wrong number of components!");
			break;
	}
}

unsigned char* Image::data() { return pixels; }