#include "math.h"
#include <algorithm>
using namespace std;

bool AABB::intersects(AABB b)
{
	if (center.x - extents.x > b.center.x + extents.x ||
		center.y - extents.y > b.center.y + extents.y ||
		center.z - extents.z > b.center.z + extents.z ||
		center.x + extents.x < b.center.x - extents.x ||
		center.y + extents.y < b.center.y - extents.y ||
		center.z + extents.z < b.center.z - extents.z)
		return false;
	else
		return true;
}

AABB AABB::intersection(AABB b)
{
	AABB res;
	Vec3 m1(center - extents), x1(center + extents), m2(b.center - b.extents), x2(b.center + b.extents);
	Vec2 i = {max(m1.x, m2.x), min(x1.x, x2.x)};
	if (i.x <= i.y) { res.extents.x = (i.y - i.x)/2; res.center.x = (i.x + i.y) / 2; }
	i = {max(m1.y, m2.y), min(x1.y, x2.y)};
	if (i.x <= i.y) { res.extents.y = (i.y - i.x)/2; res.center.y = (i.x + i.y) / 2; }
	i = {max(m1.z, m2.z), min(x1.z, x2.z)};
	if (i.x <= i.y) { res.extents.z = (i.y - i.x)/2; res.center.z = (i.x + i.y) / 2; }
	return res;
}

int ceilToPow2(int val)
{
	for (int i = 0; i < 32; ++i)
		if (1 << i >= val) return i;
	return 0;
}

static inline int log2(int x)
{
	if (x <= 0) return -1;
	int i = 0;
	while (x > 1) ++i, x /= 2;
	return i;
}

//
//Vec2::Vec2(float val): x(val), y(val) {};
//Vec2::Vec2(float x, float y): x(x), y(y) {};
//float Vec2::magnitude() const { return sqrtf(x*x + y*y); }
//float Vec2::magnitude2() const { return x*x + y*y; }
//Vec2 Vec2::normalized() const { float l = magnitude(); return Vec2(x / l, y / l); }
//void Vec2::normalize() { float l = magnitude(); x /= l; y /= l; }
//void Vec2::operator += (Vec2 rhs) { x += rhs.x; y += rhs.y; }
//void Vec2::operator -= (Vec2 rhs) { x -= rhs.x; y -= rhs.y; }
//void Vec2::operator /= (float rhs) { x /= rhs; y /= rhs; }
//void Vec2::operator *= (float rhs) { x *= rhs; y *= rhs; }
//Vec2 operator + (Vec2 lhs, Vec2 rhs) { return Vec2(lhs.x + rhs.x, lhs.y + rhs.y); }
//Vec2 operator - (Vec2 lhs, Vec2 rhs) { return Vec2(lhs.x - rhs.x, lhs.y - rhs.y); }
//Vec2 operator / (Vec2 lhs, float rhs) { return Vec2(lhs.x * rhs, lhs.y * rhs); }
//Vec2 operator * (Vec2 lhs, float rhs) { return Vec2(lhs.x / rhs, lhs.y / rhs); }
//Vec2 operator / (float lhs, Vec2 rhs) { return rhs / lhs; }
//Vec2 operator * (float lhs, Vec2 rhs) { return rhs * lhs; }
//
//Vec3::Vec3(float val): x(val), y(val), z(val) {};
//Vec3::Vec3(Vec2 val, float z): x(val.x), y(val.y), z(z) {}
//Vec3::Vec3(float x, float y, float z): x(x), y(y), z(z) {};
//float Vec3::magnitude() const { return sqrtf(x*x + y*y + z*z); }
//float Vec3::magnitude2() const { return x*x + y*y + z*z; }
//Vec3 Vec3::normalized() const { float l = magnitude(); return Vec3(x / l, y / l, z / l); }
//void Vec3::normalize() { float l = magnitude(); x /= l; y /= l; z /= l; }
//void Vec3::operator += (Vec3 rhs) { x += rhs.x; y += rhs.y; z += rhs.z; }
//void Vec3::operator -= (Vec3 rhs) { x -= rhs.x; y -= rhs.y; z -= rhs.z; }
//void Vec3::operator /= (float rhs) { x /= rhs; y /= rhs; z /= rhs; }
//void Vec3::operator *= (float rhs) { x *= rhs; y *= rhs; z *= rhs; }
//Vec3 operator + (Vec3 lhs, Vec3 rhs) { return Vec3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z); }
//Vec3 operator - (Vec3 lhs, Vec3 rhs) { return Vec3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z); }
//Vec3 operator / (Vec3 lhs, float rhs) { return Vec3(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs); }
//Vec3 operator * (Vec3 lhs, float rhs) { return Vec3(lhs.x / rhs, lhs.y / rhs, lhs.z * rhs); }
//Vec3 operator / (float lhs, Vec3 rhs) { return rhs / lhs; }
//Vec3 operator * (float lhs, Vec3 rhs) { return rhs * lhs; }
//
//Vec2i::Vec2i(int val) : x(x), y(y) {}
//Vec2i::Vec2i(int x, int y): x(x), y(y) {}
//void Vec2i::operator += (Vec2i rhs) { x += rhs.x; y += rhs.y; }
//void Vec2i::operator -= (Vec2i rhs) { x -= rhs.x; y -= rhs.y; }
//void Vec2i::operator /= (int rhs) { x /= rhs; y /= rhs; }
//void Vec2i::operator *= (int rhs) { x *= rhs; y *= rhs; }
//Vec2i operator + (Vec2i lhs, Vec2i rhs) { return Vec2i(lhs.x + rhs.x, lhs.y + rhs.y); }
//Vec2i operator - (Vec2i lhs, Vec2i rhs) { return Vec2i(lhs.x - rhs.x, lhs.y - rhs.y); }
//Vec2i operator / (Vec2i lhs, int rhs) { return Vec2i(lhs.x * rhs, lhs.y * rhs); }
//Vec2i operator * (Vec2i lhs, int rhs) { return Vec2i(lhs.x / rhs, lhs.y / rhs); }
//Vec2i operator / (int lhs, Vec2i rhs) { return rhs / lhs; }
//Vec2i operator * (int lhs, Vec2i rhs) { return rhs * lhs; }
//
//Vec3i::Vec3i(int val): x(val), y(val), z(val) {}
//Vec3i::Vec3i(Vec2i val, int z): x(val.x), y(val.y), z(z) {}
//Vec3i::Vec3i(int x, int y, int z): x(x), y(y), z(z) {}
//void Vec3i::operator += (Vec3i rhs) { x += rhs.x; y += rhs.y; z += rhs.z; }
//void Vec3i::operator -= (Vec3i rhs) { x -= rhs.x; y -= rhs.y; z -= rhs.z; }
//void Vec3i::operator /= (int rhs) { x /= rhs; y /= rhs; z /= rhs; }
//void Vec3i::operator *= (int rhs) { x *= rhs; y *= rhs; z *= rhs; }
//Vec3i operator + (Vec3i lhs, Vec3i rhs) { return Vec3i(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z); }
//Vec3i operator - (Vec3i lhs, Vec3i rhs) { return Vec3i(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z); }
//Vec3i operator / (Vec3i lhs, int rhs) { return Vec3i(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs); }
//Vec3i operator * (Vec3i lhs, int rhs) { return Vec3i(lhs.x / rhs, lhs.y / rhs, lhs.z * rhs); }
//Vec3i operator / (int lhs, Vec3i rhs) { return rhs / lhs; }
//Vec3i operator * (int lhs, Vec3i rhs) { return rhs * lhs; }

template<>
Vec2 move<Vec2>(Vec2 a, Vec2 b, float speed)
{
	auto dir = b - a;
	auto len = glm::length(dir);
	return (Vec2)(a + dir / len*speed);
}

template<>
Vec3 move<Vec3>(Vec3 a, Vec3 b, float speed)
{
	auto dir = b - a;
	auto len = glm::length(dir);
	return (Vec3)(a + dir / len*speed);
}