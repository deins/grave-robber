#pragma once
#include "debug.h"
#include <iterator>
template <typename T, size_t Capacity>
class Array
{
	char array[sizeof(T)*Capacity];
	T* endPtr;
public:
	Array(): endPtr(reinterpret_cast<T*>(array)) {};

	Array(size_t size):Array()
	{
		resize(size);
	}

	Array(size_t size, const T& fill):Array()
	{
		resize(size, fill);
	}

	Array(std::initializer_list<T> l):Array()
	{
		for (auto it = l.begin(); it != l.end(); ++it)
			new (endPtr++)T(*it); // call constructor
	}

	Array(const Array& other)
	{
		std::copy_n((const T*)(&other.array[0]), other.size(), (T*)array);
		endPtr = (T*)(&other.array[0]) + sizeof(T)*other.size();
	}

	void operator = (const Array& other)
	{
		clear();
		for (auto& v : other)
			push_back(v);
		//std::copy_n((const T*)(&other.array[0]), other.size(), (T*)array);
		//endPtr = (T*)(&other.array[0]) + sizeof(T)*other.size();
	}

	~Array()
	{
		clear();
	}

	inline size_t size() const { return (size_t)(endPtr - reinterpret_cast<const T*>(array)); }

	void resize(size_t newSize)
	{
		assert(newSize >= 0 && newSize <= Capacity);
		size_t size = this->size();
		T* newEnd = reinterpret_cast<T*>(array)+newSize;
		// Shrink
		for (; endPtr > newEnd; --endPtr)
			(endPtr - 1)->~T(); // call destructor
		// Grow
		for (; endPtr < newEnd; ++endPtr)
			new (endPtr)T(); // call constructor
	}

	//// resize without calling constructor or deconstructor, use with caution
	//void fast_unsafe_resize(int newSize)
	//{
	//	assert(newSize >= 0 && newSize <= Capacity);
	//	endPtr = ;
	//}

	void resize(size_t newSize, const T& fill)
	{
		assert(newSize >= 0 && newSize <= Capacity);
		int size = this->size();
		T* newEnd = reinterpret_cast<T*>(array)+newSize;
		// Shrink
		for (; endPtr > newEnd; --endPtr)
			(newEnd - 1)->~T(); // call destructor
		// Grow
		for (; endPtr < newEnd; ++endPtr)
			new (endPtr)T(fill); // call constructor
	}

	inline bool empty() const { return size() == 0; }
	inline bool isFull() const { return size() == Capacity; }
	inline void clear() { resize(0); }
	inline T* data() { return reinterpret_cast<T*>(array); }

	template <typename... Args>
	void emplace_back(Args... args)
	{
		assert(!isFull());
		new (endPtr++)T(args...); // call constructor
	}

	void push_back(const T& val)
	{
		emplace_back(val);
	}

	void pop_back() { assert(size() > 0);  resize(size() - 1); }

	inline T& front() { assert(size() > 0); return *reinterpret_cast<T*>(array); }
	inline const T& front() const { assert(size() > 0); return *reinterpret_cast<T*>(array); }
	inline T& back() { assert(size() > 0); return *(endPtr - 1); }
	inline const T& back() const { assert(size() > 0); return *(end - 1); }

	inline T& operator [](size_t off) { assert(off < size()); return *(reinterpret_cast<T*>(array)+off); }
	inline const T& operator [](size_t off) const { assert(off < size()); return *(reinterpret_cast<const T*>(array)+off); }


	class iterator : public std::iterator < std::random_access_iterator_tag, T >
	{
		friend class Array;
		T* ptr;
	public:
		iterator() = default;
		iterator(T* ptr): ptr(ptr) {}
		bool operator == (iterator rhs) const { return ptr == rhs.ptr; }
		bool operator != (iterator rhs) const { return ptr != rhs.ptr; }
		bool operator < (iterator rhs) const { return ptr < rhs.ptr; }
		bool operator >(iterator rhs) const { return ptr > rhs.ptr; }
		bool operator <= (iterator rhs) const { return ptr <= rhs.ptr; }
		bool operator >= (iterator rhs) const { return ptr > rhs.ptr; }

		T& operator * () { return *ptr; }
		const T& operator * () const { return *ptr; }
		T* operator -> () { return ptr; }
		const T* operator -> () const { return ptr; }

		iterator operator ++ () { return {++ptr}; }
		iterator operator -- () { return {--ptr}; }
		iterator operator ++ (int) { return {ptr++}; }
		iterator operator -- (int) { return {ptr--}; }
		iterator operator + (int rhs) { return {ptr + rhs}; }
		iterator operator - (int rhs) { return {ptr - rhs}; }
		size_t operator - (iterator rhs) { return ptr - rhs.ptr; }
		void operator += (int rhs) { ptr += rhs.ptr; }
		void operator -= (int rhs) { ptr -= rhs.ptr; }
	};

	class const_iterator : public std::iterator < std::random_access_iterator_tag, T >
	{
		friend class Array;
		T* ptr;
	public:
		const_iterator() = default;
		const_iterator(T* ptr): ptr(ptr) {}
		bool operator == (const_iterator rhs) const { return ptr == rhs.ptr; }
		bool operator != (const_iterator rhs) const { return ptr != rhs.ptr; }
		bool operator < (const_iterator rhs) const { return ptr < rhs.ptr; }
		bool operator >(const_iterator rhs) const { return ptr > rhs.ptr; }
		bool operator <= (const_iterator rhs) const { return ptr <= rhs.ptr; }
		bool operator >= (const_iterator rhs) const { return ptr > rhs.ptr; }

		const T& operator * () const { return *ptr; }
		const T* operator -> () const { return ptr; }

		const_iterator operator ++ () { return {++ptr}; }
		const_iterator operator -- () { return {--ptr}; }
		const_iterator operator ++ (int) { return {ptr++}; }
		const_iterator operator -- (int) { return {ptr--}; }
		const_iterator operator + (int rhs) { return {ptr + rhs}; }
		const_iterator operator - (int rhs) { return {ptr - rhs}; }
		int operator - (const_iterator rhs) { return ptr - rhs.ptr; }
		void operator += (int rhs) { ptr += rhs.ptr; }
		void operator -= (int rhs) { ptr -= rhs.ptr; }
	};

	iterator begin() { return {reinterpret_cast<T*>(array)}; }
	iterator end() { return {endPtr}; }
	const_iterator begin() const { return {(T*)(array)}; }
	const_iterator end() const { return {endPtr}; }
};