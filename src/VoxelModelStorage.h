#pragma once 
#include "common.h"
#include "System.h"
#include "VoxelModel.h"
#include "Array.h"

class VoxelModelStorage : public System
{
	struct Component
	{
		bool shared;
		VoxelModel* model;
		//int version;
	};
	Component models[MAX_ENTITIES];
	Array<int, MAX_ENTITIES> freeInstances;
	Array<VoxelModel, MAX_ENTITIES> instances;

	VoxelModel* createInstance(VoxelModel*);
public:
	inline VoxelModelStorage(SystemId id): System(id) {}
	virtual ~VoxelModelStorage() = default;

	VoxelModel * getModelForRead(Entity e);
	VoxelModel * getModelForWrite(Entity e);
	//VoxelModel * getModelForRead(Entity e, int version);
	//VoxelModel * getModelForWrite(Entity e, int& version);
	void createComponent(Entity e, VoxelModel*, bool shaded = true);
	virtual void onEntityDestroy(Entity e) final override;
};