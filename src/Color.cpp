#include "Color.h"

Color::Color(uint8_t b):
r(b),
g(b),
b(b),
a(255)
{
}

Color::Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a):
r(r),
g(g),
b(b),
a(a)
{
}

Color::Color(Colorf c):
r((uint8_t)(c.r * 255)),
g((uint8_t)(c.g * 255)),
b((uint8_t)(c.b * 255)),
a((uint8_t)(c.a * 255))
{
}


uint8_t Color::getBrightness() const { return (int(r) + g + b) / 3; }
float Color::getRedf() const { return r / 255.0f; }
float Color::getGreenf() const { return g / 255.0f; }
float Color::getBluef() const { return b / 255.0f; }
float Color::getAlphaf() const { return a / 255.0f; }
float Color::getBrightnessf() const { return getBrightness() / 255.0f; }
float Color::getLuminance() const { return 0.2126f*r + 0.7152f*g + 0.0722f*b; }
Colorf Color::getColorf() const { return Colorf(getRedf(), getGreenf(), getBluef(), getAlphaf()); }

Color lerp(Color a, Color b, float f)
{
	return Color(lerp(a.r, b.r, f), lerp(a.g, b.g, f), lerp(a.b, b.b, f), lerp(a.a, b.a, f));
}

// Color definitions:
const Color Color::Transparent(0, 0, 0, 0);
const Color Color::TransparentWhite(255, 255, 255, 0);
const Color Color::White(255, 255, 255);
const Color Color::Black(0, 0, 0);
const Color Color::Red(255, 0, 0);
const Color Color::Lime(0, 255, 0);
const Color Color::Blue(0, 0, 255);
const Color Color::Yellow(255, 255, 0);
const Color Color::Cyan(0, 255, 255);
const Color Color::Pink(255, 0, 255);
const Color Color::Silver(192, 192, 192);
const Color Color::Grey(128, 128, 128);
const Color Color::Maroon(128, 0, 0);
const Color Color::Olive(128, 128, 0);
const Color Color::Green(0, 128, 0);
const Color Color::Purple(128, 0, 128);
const Color Color::Teal(0, 128, 128);
const Color Color::Navy(0, 0, 128);