#pragma once
#include "common.h"
#include "debug.h"
#include "../3rd_party/pugixml/pugixml.hpp"
#include <sstream>

class Shader;
class Material;
class Image;
class VoxelModel;

void startApp();
void stop();

Entity createEntity(const char* name);
void destroyEntity(Entity entity);
void addSystemToEntity(Entity, SystemId);
bool isEntityValid(Entity);

int getCurrentFrame();

// Config
extern pugi::xml_document config;
template <typename T>
T getConfigParameter(const char*path)
{
	auto node = config.child("config").first_element_by_path(path);
	if (node.empty()) fatalError("config error, path no found: %s", path);
	T val;
	std::istringstream ss(node.child_value());
	ss >> val;
	if (!ss) fatalError("config failed to parse value <%s> for path: %s", typeid(T).name(), path);
	return val;
}

// Assets
Shader* getShader();
Shader* getShader(const char* name);
Material* getMaterial();
Material* getMaterial(const char* name);
Image* getImage(const char* name);
Image* getOrLoadImage(const char* name);
VoxelModel* getVoxelModel(const char* name);

// Systems
class Renderer;
extern Renderer* renderer;

class Collision;
extern Collision* collision;

class VoxelModelStorage;
extern VoxelModelStorage* vmStorage;

class TransformStorage;
extern TransformStorage* transformStorage;