﻿#pragma once 
#include "common.h"
#include "platform.h"

class Shader;

class ShaderStage : NonCopyable
{
public:
	friend class Shader;
	enum Type
	{
		Undefined = 0,
		Vertex = GL_VERTEX_SHADER,
		Geometry = GL_GEOMETRY_SHADER,
		Fragment = GL_FRAGMENT_SHADER,
		Compute = GL_COMPUTE_SHADER,
		TesselationControl = GL_TESS_CONTROL_SHADER,
		TesselationEvaluation = GL_TESS_EVALUATION_SHADER
	};

	void loadFromFile(const char* filename);

	ShaderStage() = default;
	~ShaderStage();
private:
	void createFromSource(const char* source, Type type);
	const char* getTypeName(Type t) const;
	Type type = Type::Undefined;
	GLuint shader = 0;
};

class Shader : NonCopyable
{
public:
	/* Uniform & Attribute type values:
	GL_FLOAT, GL_FLOAT_VEC2, GL_FLOAT_VEC3, GL_FLOAT_VEC4, GL_INT, GL_INT_VEC2, GL_INT_VEC3, GL_INT_VEC4, GL_BOOL, GL_BOOL_VEC2, GL_BOOL_VEC3, GL_BOOL_VEC4, GL_FLOAT_MAT2, GL_FLOAT_MAT3, GL_FLOAT_MAT4, GL_SAMPLER_2D, or GL_SAMPLER_CUBE
	*/
	struct Attribute
	{
		char name[32];
		GLint location;
		GLenum type;
		int size; // array size
		bool operator < (const std::string&) const; // for binary search
		bool operator == (const std::string&) const; // for search
	};

	struct Uniform
	{
		char name[32];
		GLint location;
		GLenum type;
		int size; // array size
		bool operator < (const std::string&) const; // for binary search
		bool operator == (const std::string&) const; // for search
	};

	Shader() = default;
	~Shader();

	void addStage(ShaderStage* stage);
	void link();
	GLint  getUniformLocation(const std::string& name) const;
	GLint  getAttributeLocation(const std::string& name) const;
	const std::vector<Uniform>& getUniforms() const;
	const std::vector<Attribute>& getAttributes() const;
	void bind();
	static void unbind();
private:
	std::vector<Attribute> attributes;
	std::vector<Uniform> uniforms;
	GLuint shaderProgram = 0;
	bool linked = false;
	static GLuint boundShaderProgram;
};
