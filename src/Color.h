#pragma once
#include "math.h"
#include <cinttypes>

struct Color
{
	uint8_t r, g, b, a;
	Color() = default;
	Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255);
	Color(uint8_t brightness);
	Color(Colorf);

	float getRedf() const;
	float getGreenf() const;
	float getBluef() const;
	float getAlphaf() const;
	float getBrightnessf() const;
	float getLuminance() const;
	uint8_t getBrightness() const;
	Colorf getColorf() const;

	// 16 basic colors + transparent
	static const Color White;
	static const Color Black;
	static const Color Red;
	static const Color Lime;
	static const Color Blue;
	static const Color Yellow;
	static const Color Cyan;
	static const Color Pink;
	static const Color Silver;
	static const Color Grey;
	static const Color Maroon;
	static const Color Olive;
	static const Color Green;
	static const Color Purple;
	static const Color Teal;
	static const Color Navy;
	static const Color Transparent;
	static const Color TransparentWhite;
};

Color lerp( Color a, Color b, float v);