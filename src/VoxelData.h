#pragma once 
#include "Color.h"

//	VoxelInfo in 1B color alpha layout:
// first 6 bits visible faces
// 7th bit is solid

//	idea about more compact layout that skips >3 visible faces: 5 bits voxel status (values: 0 empty; 1 solid fully visible; 2-7 solid 1 face visible; 8-19 solid 2 faces visible; 20-27 solid 3 faces visible, 31 - unknown

class VoxelData
{
	Color color;
public:
	inline VoxelData();
	inline VoxelData(Color);

	inline Color getColor();
	inline void setColor(Color);
	inline Colorf getColorf();
	inline bool isEmpty() const;
	inline bool isSolid()const;
	inline bool isVisible()const;
	inline bool isFullyVisible() const;
	inline void setVisibleFaces(UInt8);
	inline void erase();
	//Vec3 getNormal() const;
};

inline VoxelData::VoxelData():
color(0, 0, 0, 0)
{
}

inline VoxelData::VoxelData(Color c) : color(c.r, c.g, c.b, 0xFF) { assert(isSolid() && isVisible()); }

inline Color VoxelData::getColor() { return color; }

inline void VoxelData::setColor(Color c)
{
	color.r = c.r;
	color.g = c.g;
	color.b = c.b;
}

inline bool VoxelData::isVisible() const { return (color.a & 0x3F) != 0; }

inline bool VoxelData::isSolid() const { return (color.a & 0x40) != 0; }

inline bool VoxelData::isFullyVisible() const { return (color.a & 0x3F) == 0x3F; }

inline Colorf VoxelData::getColorf()
{
	return color.getColorf();
}

inline void VoxelData::setVisibleFaces(UInt8 n)
{
	color.a = (color.a & 0xC0) | n;
}

inline bool VoxelData::isEmpty() const
{
	return !isSolid();
}

inline void VoxelData::erase()
{
	color.a = 0;
}
