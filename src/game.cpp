#include "common.h"
#include "platform.h"
#include "game.h"
#include "math.h"
#include "Renderer.h"
#include "Color.h"
#include "debug.h"
#include "Array.h"
#include "Shader.h"
#include "Image.h"
#include "Texture.h"
#include "Material.h"
#include "VoxelModel.h"
#include "World.h"
#include "System.h"
#include "Collision.h"
#include "VoxelModelStorage.h"
#include "TransformStorage.h"
#include <pugixml/pugixml.hpp> 
#include <algorithm>
#include <unordered_map>

// Systems
Renderer* renderer;
Collision* collision;
VoxelModelStorage* vmStorage;
TransformStorage* transformStorage;

pugi::xml_document config;

namespace
{
	bool isRunning = false;
	int frame = 0;
	World world;

	Array<System*, 8> systems;

	// Entity data
	Array<int, MAX_ENTITIES> freeEntities;
	Array<Entity::Version, MAX_ENTITIES> entityVersion;
	Array<char[MAX_ENTITY_NAME_LENGTH], MAX_ENTITIES> entityName;
	Array<UInt32, MAX_ENTITIES> entityComponents;

	// Assets
	Array<Shader, 32> shaders;
	std::unordered_map<std::string, Shader*> shaderIndex;
	Shader* defaultShader = nullptr;
	std::string imagePath;
	Array<Image, 256> images;
	std::unordered_map<std::string, Image*> imageIndex;
	//Array<Texture, 256> textures;
	//std::unordered_map<std::string, Texture*> textureIndex;
	Array<Material, 32> materials;
	std::unordered_map<std::string, Material*> materialIndex;
	Material* defaultMaterial = nullptr;
	Array<VoxelModel, 32> voxelModels;
	std::unordered_map<std::string, VoxelModel*> voxelModelIndex;

	void mainLoop()
	{
		using namespace platform;
		isRunning = true;
		UInt64 lastFrame = getMicroseconds();
		const float maxDeltaT = 1 / 15.0f;
		float deltaT = 1 / 60.0f;

		Transform t;
		for (int i = 0; i < 16; ++i)
			for (int j = 0; j < 16; ++j)
			{
				//if (i != 0 && j !=0 ) continue;
				t.pos.x = i * 128.0f;
				t.pos.z = j * 128.0f;

				Entity e = createEntity("Entity");
				transformStorage->getTransform(e) = t;
				vmStorage->createComponent(e, &voxelModels[(i + j) % (voxelModels.size() - 2)]);
				renderer->createVoxelModel(e);
				collision->createVoxelModelCollider(e);
			}

		world.init();
		platform::display();
		while (isRunning)
		{
			++frame;
			processEvents();

			world.update(deltaT);
			renderer->render();

			UInt64 t = getMicroseconds();
			deltaT = (t - lastFrame) / 1000000.0f;
			float maxDeltaT = 1 / 20.0f;
			if (frame % 25 == 0)
				platform::setTitle((std::string("fps: ") + toString(1.0f / deltaT)).data());
			if (deltaT > maxDeltaT) deltaT = maxDeltaT;
			lastFrame = t;
		}
	}
}

void startApp()
{
	{	// Read config
		auto buffer = platform::readAssetFile("../config.xml");
		auto result = config.load_buffer(buffer.data(), buffer.size()); // make
		if (result.status != pugi::status_ok) fatalError("Can't open config file !");
	}

	{// Initialize Systems
		renderer = new Renderer(0);
		systems.emplace_back(renderer);
		collision = new Collision(1);
		systems.emplace_back(collision);
		vmStorage = new VoxelModelStorage(2);
		systems.emplace_back(vmStorage);
		transformStorage = new TransformStorage(3);
		systems.emplace_back(transformStorage);
	}

	// Init systems
	for (auto* s : systems) s->init();

	platform::setVsync(getConfigParameter<bool>("graphics/vsync"));
	{ // load assets
		log("Loading assets");
		using namespace pugi;
		pugi::xml_document doc;
		auto buffer = platform::readAssetFile("assets.xml");
		auto result = doc.load_buffer_inplace(buffer.data(), buffer.size());
		if (result.status != status_ok) fatalError("Can't open file 'assets.xml'!");
		auto assets = doc.child("assets");
		if (!doc) fatalError("assets.xml can't find root node 'assets'!");

		{ // Images
			auto node = assets.child("images");
			assert(!node.empty());
			imagePath = node.attribute("path").as_string("/");
			assert(imagePath.back() == '/' || imagePath.back() == '\\');

			for (auto& c : node)
			{
				assert(strcmp(c.name(), "image") == 0);
				std::string name = c.attribute("name").as_string();
				std::string filename = c.attribute("file").as_string();
				assert(!filename.empty());
				if (name.empty()) name = filename;
				images.emplace_back();
				if (!imageIndex.emplace(name, &images.back()).second)
					fatalError("image duplicate!");
				images.back().loadFromFile((imagePath + filename).c_str());
			}
			std::string def = node.attribute("default").as_string();
			if (!def.empty())
			{
				assert(materialIndex.find(def) != materialIndex.end());
				defaultMaterial = materialIndex[def];
				defaultMaterial->bind();
			}
		}

		{ // Textures

		}

		{	// Shaders
			Array<ShaderStage, 128> shaderStages;
			std::unordered_map<std::string, ShaderStage*> shaderStageIndex;

			auto node = assets.child("shaders");
			assert(!node.empty());
			std::string path = node.attribute("path").as_string("/");
			assert(path.back() == '/' || path.back() == '\\');
			for (auto& p : node)
			{
				assert(strcmp(p.name(), "shader") == 0);
				std::string name = p.attribute("name").as_string();
				assert(!name.empty());
				shaders.emplace_back();
				auto siIdx = shaderIndex.emplace(name, &shaders.back());
				assert(siIdx.second);
				for (auto& stage : p)
				{
					assert(strcmp(stage.name(), "stage") == 0);
					std::string filename = stage.attribute("file").as_string("");
					assert(!filename.empty());
					{	// find or load shader stage
						auto it = shaderStageIndex.find(filename);
						if (it == shaderStageIndex.end())
						{
							shaderStages.emplace_back();
							it = shaderStageIndex.emplace(filename, &shaderStages.back()).first;
							shaderStages.back().loadFromFile((path + filename).data());
						}
						shaders.back().addStage(it->second);
					}
				}
				shaders.back().link();
			}

			std::string def = node.attribute("default").as_string();
			if (!def.empty())
			{
				assert(shaderIndex.find(def) != shaderIndex.end());
				defaultShader = shaderIndex[def];
			}
		}
		glDebugCheck();

		{	// Materials
			auto node = assets.child("materials");
			assert(!node.empty());
			std::string path = node.attribute("path").as_string("/");
			assert(path.back() == '/' || path.back() == '\\');

			for (auto& c : node)
			{
				assert(strcmp(c.name(), "material") == 0);
				std::string name = c.attribute("name").as_string();
				assert(!name.empty());
				std::string shader = c.attribute("shader").as_string();
				assert(!shader.empty());

				materials.emplace_back();
				if (!materialIndex.emplace(name, &materials.back()).second)
					fatalError("material duplicate!");
				Material& m = materials.back();
				auto blendMode = c.attribute("blend-mode").as_string("none");
				m.setBlendMode(blendMode);
				m.setShader(shaderIndex[shader]);

				for (auto& pn : c)
				{
					// Parameters
					if (strcmp(pn.name(), "float") == 0)
					{
						float val = pn.attribute("value").as_float(pn.attribute("x").as_float(NaN));
						if (val == NaN) warn("parameter missing value!");
						if (!m.trySetFloat(pn.name(), val))
							warn("material can't set non existing parameter!");
					}
					else if (strcmp(pn.name(), "vec2") == 0)
					{
						float x = pn.attribute("x").as_float(NaN);
						float y = pn.attribute("y").as_float(NaN);
						if (x == NaN) warn("parameter missing value x");
						if (y == NaN) warn("parameter missing value y");
						if (!m.trySetVec2(pn.name(), Vec2(x, y)))
							warn("material can't set non existing parameter!");
					}
					else if (strcmp(pn.name(), "vec3") == 0)
					{
						float x = pn.attribute("x").as_float(NaN);
						float y = pn.attribute("y").as_float(NaN);
						float z = pn.attribute("z").as_float(NaN);
						if (x == NaN) warn("parameter missing value x");
						if (y == NaN) warn("parameter missing value y");
						if (z == NaN) warn("parameter missing value z");
						if (!m.trySetVec3(pn.name(), Vec3(x, y, z)))
							warn("material can't set non existing parameter!");
					}
					else if (strcmp(pn.name(), "vec4") == 0)
					{
						float x = pn.attribute("x").as_float(NaN);
						float y = pn.attribute("y").as_float(NaN);
						float z = pn.attribute("z").as_float(NaN);
						float w = pn.attribute("w").as_float(NaN);
						if (x == NaN) warn("parameter missing value x");
						if (y == NaN) warn("parameter missing value y");
						if (z == NaN) warn("parameter missing value z");
						if (w == NaN) warn("parameter missing value w");
						if (!m.trySetVec4(pn.name(), Vec4(x, y, z, w)))
							warn("material can't set non existing parameter!");
					}
					else warn("Unknown parameter type '%s'", pn.name());
				}
			}

			{ // Prefabs
				auto node = assets.child("prefabs");
				assert(!node.empty());
				for (auto& p : node)
				{
					if (strcmp(p.name(), "voxel_sprite") == 0)
					{
						std::string name = p.attribute("name").as_string();
						assert(!name.empty());
						log("load voxel_sprite %s", name);
						startProfiling(voxel_sprite_load);
						voxelModels.emplace_back();
						if (!voxelModelIndex.emplace(name, &voxelModels.back()).second) 
							fatalError("Voxelmodel with same name already exists!");
						for (auto& stage : p)
						{
							if (strcmp(stage.name(), "carve") == 0)
							{
								voxelModels.back().createAndCarve(
									getOrLoadImage(stage.child("front").attribute("file").as_string("")),
									getOrLoadImage(stage.child("left").attribute("file").as_string("")),
									getOrLoadImage(stage.child("top").attribute("file").as_string(""))
									);
							}
							else if (strcmp(stage.name(), "paint") == 0)
							{
								voxelModels.back().paint(
									getOrLoadImage(stage.child("front").attribute("file").as_string("")),
									getOrLoadImage(stage.child("left").attribute("file").as_string("")),
									getOrLoadImage(stage.child("top").attribute("file").as_string(""))
									);
							}
							else warn("unknown voxel-model stage %s", stage.name());
						}
						endProfiling(voxel_sprite_load);
					}
					else warn("unknown prefab type %s", p.name());
				}

				std::string def = node.attribute("default").as_string();
				if (!def.empty())
				{
					assert(shaderIndex.find(def) != shaderIndex.end());
					defaultShader = shaderIndex[def];
				}
			}

			std::string def = node.attribute("default").as_string();
			if (!def.empty())
			{
				assert(materialIndex.find(def) != materialIndex.end());
				defaultMaterial = materialIndex[def];
				defaultMaterial->bind();
			}
		}
	}
	mainLoop();
}

int getCurrentFrame()
{
	return frame;
}

Shader* getShader()
{
	return (defaultShader);
}

Shader* getShader(const char* name)
{
	auto it = shaderIndex.find(name);
	assert(it != shaderIndex.end());
	return (it->second);
}

Material* getMaterial()
{
	return (defaultMaterial);
}

Material* getMaterial(const char* name)
{
	auto it = materialIndex.find(name);
	if (it != materialIndex.end())
		return (it->second);
	else return nullptr;
}

VoxelModel* getVoxelModel(const char* name)
{
	auto it = voxelModelIndex.find(name);
	if (it != voxelModelIndex.end())
		return (it->second);
	else return nullptr;
}

Image* getImage(const char* name)
{
	auto it = imageIndex.find(name);
	if (it != imageIndex.end())
		return (it->second);
	else return nullptr;
}

Image* getOrLoadImage(const char* name)
{
	if (*name == 0) return nullptr;
	Image* ptr = getImage(name);
	if (ptr) return ptr;
	images.emplace_back();
	if (imageIndex.emplace(name, &images.back()).second)
		images.back().loadFromFile((imagePath + name).c_str());
	return &(images.back());
}

void stop()
{
	isRunning = false;
}

Entity createEntity(const char* name)
{
	Entity::Idx idx;
	if (!freeEntities.empty())
	{
		idx = freeEntities.back();
		freeEntities.pop_back();
	}
	else
	{
		idx = (int) entityVersion.size();
		entityVersion.emplace_back(0);
		entityComponents.push_back(0);
		entityName.emplace_back();
	}
	strcpy_s(entityName[idx], name);
	entityVersion[idx]++;
	return {idx, entityVersion[idx]};
}

void destroyEntity(Entity e)
{
	log("destroy entity %d, %d", e.idx, e.version);
	freeEntities.push_back(e.idx);
	for (int i = 0; i < 32; ++i)
	{
		if ((entityComponents[e.idx] & (1 << i)) != 0)
		{
			systems[i]->onEntityDestroy(e);
		}
	}
	entityComponents[e.idx] = 0;
}

void addSystemToEntity(Entity e, SystemId s)
{
	entityComponents[e.idx] |= (1 << s);
}

bool isEntityValid(Entity e)
{
	return entityVersion[e.idx] == e.version;
}