#pragma once
#include <sstream>
#include "platform.h"

#define log(x,...) do{auto str = prepareLog(__FILE__,__LINE__,parametrizeString((x), __VA_ARGS__ ),false); platform::logMessage(str.data());} while(0)
#define logWarning(x,...) do{auto str = prepareLog(__FILE__,__LINE__,parametrizeString((x), __VA_ARGS__ ),true);  platform::logWarningMessage(str.data());} while(0)
#define logError(x,...) do{auto str = prepareLog(__FILE__,__LINE__,parametrizeString((x), __VA_ARGS__ ),true);  platform::logErrorMessage(str.data());} while(0)
#define fatalError(x,...) do{	\
auto str = prepareLog(__FILE__,__LINE__,parametrizeString((x), __VA_ARGS__ ),true); platform::logErrorMessage(str.data());	\
str = parametrizeString((x), __VA_ARGS__ );	platform::showMessageBox("ERROR",str.data(),MessageBoxType::Error);	abort();} while(0)

#ifdef assert
#undef assert
#endif

#ifdef DEBUG
#define warn(x,...) do{ fatalError("%s",prepareLog(__FILE__,__LINE__,parametrizeString((x), __VA_ARGS__ ),true));}while(0)
extern const char* glErrorString(GLenum err);
#define assert(check) if (!(check)) {fatalError("Assert (" #check ")! ");}
#define glDebugCheck() do{auto err = glGetError(); if (err!=GL_NO_ERROR) fatalError("GL error: %*",glErrorString(err));}while(0)
#define startProfiling(x)  log(#x " starts"); long long x## = platform::getMicroseconds()
#define endProfiling(x)  log( #x  " finished in %d milisec",(platform::getMicroseconds()- x##)/1000); 
#else
#define startProfiling(x)
#define endProfiling(x)
#define warn(x,...) do{auto str = prepareLog(__FILE__,__LINE__,parametrizeString((x), __VA_ARGS__ ),true);  platform::logWarningMessage(str.data()); platform::showMessageBox("WARNING",str.data(),MessageBoxType::Warning); }while(0)
#define assert(check) 
#define glDebugCheck()
#endif

namespace
{
	template <typename T>
	std::string toString(T val)
	{
		std::ostringstream ss;
		ss << val;
		return ss.str();
	}

	template<>
	std::string toString(const char* str)
	{
		return std::string(str);
	}

	std::string parametrizeString(const char* str)
	{
		return std::string(str);
	}

	template <typename... Args>
	std::string parametrizeString(const char* str, Args... args)
	{
		int idx = 0;
		const char *last = str;
		std::string argStr[] = {toString(args)...};
		std::string res;

		for (const char* ptr = str;;)
		{
			auto add = [&res, &last, &ptr]
			{
				res.append(last, ptr - last - 1);
				last = ptr + 1;
			};
			const char* nptr = strchr(ptr, '%'); // returns 0 if can't find
			if (nptr == nullptr)
			{
				// char not found (nullptr)
				ptr += strlen(ptr) + 1;
				if (ptr - last - 1 > 0) add();
				break;
			}
			ptr = nptr + 1;
			if (*ptr == 0)
				fatalError("There can't be str which ends with single '%', do you mean '%%'?");

			switch (*ptr)
			{
				case('%') :
					break;

				case('f') :
				case('d') :
				case('u') :
				case('s') :
				case ('*') : {
					add();
					int s = sizeof...(args);
					assert(idx < s);
					res += argStr[idx++];
					break;
				}

				case('\n') :
					add();
					break;

				default:
					if (*ptr < '0' || *ptr > '9')
						fatalError("str unknown argument! To display '%' type '%%' instead!");

					add();
					res += argStr[*ptr - '0'];
					break;
			}
			++ptr;
		}
		return res;
	}

	std::string prepareLog(const char* file, int line, std::string message, bool showSource = true)
	{
		std::ostringstream ss;
		ss << message;

		if (showSource)
		{
			if (message.size() < 32)
				ss << std::string(32 - message.size(), ' ');
			ss << file << ':' << line;
		}
		std::string str = ss.str();
		{ // replace '\n' with ' '
			for (const char *ptr = strchr(str.c_str(), '\n'); ptr != nullptr; ptr = strchr(ptr, '\n'))
				*const_cast<char*> (ptr) = ' '; // WARNING: const_cast not safe, but works with known stl::string implementation
		}
		return str;
	}
}
