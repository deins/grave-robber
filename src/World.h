#pragma once
#include "common.h"
#include "math.h"
#include "Array.h"
#include <unordered_map>
#include <vector>

#define CellSize 128
#define WorldCellsX 32
#define WorldCellsY 32
#define WorldSizeX (WorldCellsX*WorldCellsX)
#define WorldSizeY (WorldCellsY*WorldCellsY)

class World
{
public:
private:

	class VoxelWorld
	{

	};
	Array<Array<Entity, 8092>, WorldCellsX * WorldCellsY> grid;

	//std::unordered_map<Int64, std::vector<Entity>> grid;
	//std::vector<Entity>* getCell(int x, int y);
	//std::vector<Entity>& getOrCreateCell(int x, int y);
	Vec3 camVelocity;
	Vec3 camOldPos;
	Vec2 mouseSensitivity;
	Vec3 camAngles;
	Entity editEnt;
public:
	Array<Entity, 8092>& World::getCell(int x, int y);

	void init();
	void update(float dt);
	Entity createEntity(const char* name);
	void destroyEntity(Entity);

};