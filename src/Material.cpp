#include "Material.h"
#include "debug.h"
//#include "pugixml.hpp"
#include <algorithm>


Material* Material::boundMaterial = nullptr;

Material::~Material()
{
	if (boundMaterial == this)
		Material::unbind();
}

void Material::setShader(Shader* shader)
{
	this->shader = shader;
	// TODO: keep old parameter values 
	for (auto& it : fParams) it.location = -1;
	for (auto& it : f2Params) it.location = -1;
	for (auto& it : f3Params) it.location = -1;
	for (auto& it : f4Params) it.location = -1;
	for (auto& it : tParams) it.location = -1;
	for (auto& it : m4Params) it.location = -1;

	for (const auto& u : shader->getUniforms())
	{
		/* Uniform & Attribute type values:
		GL_FLOAT, GL_FLOAT_VEC2, GL_FLOAT_VEC3, GL_FLOAT_VEC4, GL_INT, GL_INT_VEC2, GL_INT_VEC3, GL_INT_VEC4, GL_BOOL, GL_BOOL_VEC2, GL_BOOL_VEC3, GL_BOOL_VEC4, GL_FLOAT_MAT2, GL_FLOAT_MAT3, GL_FLOAT_MAT4, GL_SAMPLER_2D, or GL_SAMPLER_CUBE
		*/
		switch (u.type)
		{
			case (GL_FLOAT) :
				fParams.emplace_back(u.location, 0.0f, u.name);
				break;
			case(GL_FLOAT_VEC2) :
				f2Params.emplace_back(u.location, Vec2(), u.name);
				break;
			case(GL_FLOAT_VEC3) :
				f3Params.emplace_back(u.location, Vec3(), u.name);
				break;
			case(GL_FLOAT_VEC4) :
				f4Params.emplace_back(u.location, Vec4(), u.name);
				break;
			case(GL_SAMPLER_2D) :
				tParams.emplace_back(u.location, nullptr, u.name);
				break;
			case(GL_FLOAT_MAT4) :
				m4Params.emplace_back(u.location, Mat4(), u.name);
				break;
			default:
				//assert(false);
				warn("Unknown Shader Parameter: %d", u.type);
				break;
		}
	}

	// remove old parameters
	std::remove_if(fParams.begin(), fParams.end(), [](const FP& p) { return p.location < 0; });
	std::remove_if(f2Params.begin(), f2Params.end(), [](const F2P& p) { return p.location < 0; });
	std::remove_if(f3Params.begin(), f3Params.end(), [](const F3P& p) { return p.location < 0; });
	std::remove_if(f4Params.begin(), f4Params.end(), [](const F4P& p) { return p.location < 0; });
	std::remove_if(tParams.begin(), tParams.end(), [](const TP& p) { return p.location < 0; });
	std::remove_if(m4Params.begin(), m4Params.end(), [](const M4P& p) { return p.location < 0; });
}

Shader* Material::getShader()
{
	return shader;
}

bool Material::trySetFloat(const char* name, float value)
{
	auto it = find(fParams.begin(), fParams.end(), name);
	if (it == fParams.end()) return false;
	it->value = value;
	if (boundMaterial == this)
	{
		glUniform1f(it->location, value);
		glDebugCheck();
	}
	return true;
}

bool Material::trySetVec2(const char* name, const Vec2 value)
{
	auto it = find(f2Params.begin(), f2Params.end(), name);
	if (it == f2Params.end()) return false;
	it->value = value;
	if (boundMaterial == this) {
		glUniform2f(it->location, value.x, value.y);
		glDebugCheck();
	}
	return true;
}

bool Material::trySetVec3(const char* name, const Vec3 value)
{
	auto it = find(f3Params.begin(), f3Params.end(), name);
	if (it == f3Params.end()) return false;
	it->value = value;
	if (boundMaterial == this) {
		glUniform3f(it->location, value.x, value.y, value.z);
		glDebugCheck();
	}
	return true;
}

bool Material::trySetVec4(const char* name, const Vec4& value)
{
	auto it = find(f4Params.begin(), f4Params.end(), name);
	if (it == f4Params.end()) return false;
	it->value = value;
	if (boundMaterial == this) {
		glUniform4f(it->location, value.x, value.y, value.z, value.q);
		glDebugCheck();
	}
	return true;
}

bool Material::trySetTexture(const char* name, Texture* value)
{
	auto it = find(tParams.begin(), tParams.end(), name);
	if (it == tParams.end()) return false;
	it->value = value;
	if (boundMaterial == this)
	{
		Texture::bind(*value, it->location);
	}
	return true;
}

bool Material::trySetMat4(const char* name, const Mat4& value)
{
	auto it = find(m4Params.begin(), m4Params.end(), name);
	if (it == m4Params.end()) return false;
	it->value = value;
	if (boundMaterial == this) {
		glUniformMatrix4fv(it->location, 1, false, (const GLfloat*)glm::value_ptr(value));
		glDebugCheck();
	}
	return true;
}

bool Material::trySetColor(const char* name, Color value)
{
	return trySetVec4(name, Vec4(value.r / 255.0f, value.g / 255.0f, value.b / 255.0f, value.a / 255.0f));
}

void Material::setFloat(const char* name, float value)
{
	if (!trySetFloat(name, value)) warn("Material can't set value!");

}

void Material::setVec2(const char* name, const Vec2 value)
{
	if (!trySetVec2(name, value)) warn("Material can't set value!");

}

void Material::setVec3(const char* name, const Vec3 value)
{
	if (!trySetVec3(name, value)) warn("Material can't set value!");

}

void Material::setVec4(const char* name, const Vec4& value)
{
	if (!trySetVec4(name, value)) warn("Material can't set value!");

}

void Material::setTexture(const char* name, Texture* value)
{
	if (!trySetTexture(name, value)) warn("Material can't set value!");

}

void Material::setMat4(const char* name, const Mat4& value)
{
	if (!trySetMat4(name, value)) warn("Material can't set value '%s'!", name);
}

void Material::setColor(const char* name, Color value)
{
	if (!trySetColor(name, value)) warn("Material can't set value!");
}

float Material::getFloat(const char* name) const
{
	auto it = find(fParams.begin(), fParams.end(), name);
	assert(it != fParams.end());
	return it->value;
}

Vec2 Material::getVec2(const char* name) const
{
	auto it = find(f2Params.begin(), f2Params.end(), name);
	assert(it != f2Params.end());
	return it->value;
}

Vec3 Material::getVec3(const char* name) const
{
	auto it = find(f3Params.begin(), f3Params.end(), name);
	assert(it != f3Params.end());
	return it->value;
}

Vec4 Material::getVec4(const char* name) const
{
	auto it = find(f4Params.begin(), f4Params.end(), name);
	assert(it != f4Params.end());
	return it->value;
}

Texture* Material::getTexture(const char* name) const
{
	auto it = find(tParams.begin(), tParams.end(), name);
	assert(it != tParams.end());
	return it->value;
}

Color Material::getColor(const char* name) const
{
	Vec4 c = getVec4(name);
	return Color(int(c.r*255.0f), int(c.g*255.0f), int(c.b*255.0f), int(c.a*255.0f));
}

void Material::bind()
{
	if (boundMaterial == this) return;
	boundMaterial = this;
	shader->bind();
	for (const auto& p : fParams) { glUniform1f(p.location, p.value); }
	for (const auto& p : f2Params) { glUniform2f(p.location, p.value.x, p.value.y); }
	for (const auto& p : f3Params) { glUniform3f(p.location, p.value.x, p.value.y, p.value.z); }
	for (const auto& p : f4Params) { glUniform4f(p.location, p.value.x, p.value.y, p.value.z, p.value.w); }
	for (const auto& p : m4Params)
	{
		glUniformMatrix4fv(p.location, 1, false, (const GLfloat*)glm::value_ptr(p.value));
	}

	// 2D Texture binding
	int i = 0;
	for (const auto& p : tParams)
	{
		glUniform1i(p.location, i);
		Texture::bind(*p.value, i);
		++i;
	}

	// Blending
	switch (blendMode)
	{
		case (BlendMode::None) :
			glDisable(GL_BLEND);
			break;
		case (BlendMode::Alpha) :
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			break;
		case (BlendMode::PremultipliedAlpha) :
			glEnable(GL_BLEND);
			glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
			break;
		case (BlendMode::Add) :
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
			break;
		case (BlendMode::Multiply) :
			glEnable(GL_BLEND);
			glBlendFunc(GL_DST_COLOR, GL_ZERO);
			break;
	}
}

void Material::unbind()
{
	boundMaterial = nullptr;
	// TODO: should we do something else too ?
}

void Material::setBlendMode(BlendMode mode)
{
	if (blendMode == mode) return;
	blendMode = mode;
	if (this == boundMaterial)
	{
		// todo: switch gl blend mode too
	}
}

void Material::setBlendMode(const char* name)
{
	BlendMode newMode = BlendMode::None;
	if (strcmp(name, "none") == 0) newMode = BlendMode::None;
	else if (strcmp(name, "alpha") == 0) newMode = BlendMode::Alpha;
	else if (strcmp(name, "add") == 0) newMode = BlendMode::Add;
	else if (strcmp(name, "multiply") == 0) newMode = BlendMode::Multiply;
	else if (strcmp(name, "premultiplied alpha") == 0) newMode = BlendMode::PremultipliedAlpha;
	else warn("unknown blending mode: %s", name);
	setBlendMode(newMode);
}

Material::BlendMode Material::getBlendMode() const
{
	return blendMode;
}