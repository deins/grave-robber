#pragma once
#include <SDL.h>
#include "common.h"
#include "math.h"
#include <gl3w/gl3w.h>
#include <vector>
// undef some win api defines
#undef far
#undef near

enum MessageBoxType
{
	Normal,
	Warning,
	Error
};

enum MouseButton
{
	Left = SDL_BUTTON_LEFT,
	Right = SDL_BUTTON_RIGHT,
	Middle = SDL_BUTTON_MIDDLE
};

typedef unsigned Key;

namespace platform
{
	void processEvents();
	void display();
	void makeContextCurrent();
	void releaseContext();

	bool setVsync(bool state);
	void showCursor(bool state);
	void setTitle(const char*);
	::Vec2 getWindowSize();
	UInt64 getMicroseconds();

	Key getKey(const char*);
	bool isKeyDown(Key);
	bool isKeyUp(Key);
	bool isKeyActive(Key);
	Vec2 getMousePos();
	Vec2 getMouseDelta();
	bool isMouseButtonDown(MouseButton);
	bool isMouseButtonUp(MouseButton);
	bool isMouseButtonActive(MouseButton);

	std::vector<UInt8>  readAssetFile(const char* filename);

	void logMessage(const char*);
	void logWarningMessage(const char*);
	void logErrorMessage(const char*);

	void showMessageBox(const char* title, const char* message, MessageBoxType type = MessageBoxType::Normal);
}