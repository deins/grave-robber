#include "Texture.h"
#include "debug.h"
#include <algorithm>


GLuint Texture::boundTexture[8] = {0, 0, 0, 0, 0, 0, 0, 0};

Texture::Texture(Image& img)
{
	glGenTextures(1, &texture);
	assert(texture > 0);
	bind(*this);
	GLenum formats[] = {GL_RED, GL_RG, GL_RGB, GL_RGBA};
	assert(img.getComponents() > 0 && img.getComponents() <= 4);
	assert(img.getWidth() > 0 && img.getHeight() > 0);
	glTexImage2D(GL_TEXTURE_2D, 0, formats[img.getComponents()], img.getWidth(), img.getHeight(), 0, formats[img.getComponents()], GL_UNSIGNED_BYTE, img.data());
}

Texture::~Texture()
{
	if (texture > 0) glDeleteTextures(1, &texture);
}

void Texture::setFiltering(std::string mode)
{
	std::transform(mode.begin(), mode.end(), mode.begin(), ::tolower);
	if (mode.empty() || mode == "none" || mode == "off") setFiltering(None);
	else if (mode == "smooth") setFiltering(Smooth);
	else if (mode == "nearest") setFiltering(Nearest);
	else if (mode == "bilinear") setFiltering(Bilinear);
	else if (mode == "trilinear") setFiltering(Trilinear);
	else warn("Unkown blending mode: %s", mode.c_str());
}

void Texture::setFiltering(FilteringMode mode)
{
	if (mode == filtering) return;
	bind(*this);
	filtering = mode;
	switch (filtering)
	{
		case (None) :
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			break;

		case (Smooth) :
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			break;

		case(Nearest) :
			genMipMaps();
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			break;

		case (FilteringMode::Bilinear) :
			genMipMaps();
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			break;

		case (FilteringMode::Trilinear) :
		{
			genMipMaps();
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			break;
		}
	}
}

void Texture::genMipMaps()
{
	if (!mipmaps)
	{
		mipmaps = true;
		bind(*this);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
}

int Texture::getWidth() const
{
	return width;
}

int Texture::getHeight() const
{
	return height;
}

void Texture::bind(const Texture& texture, int slot)
{
	if (texture.texture == boundTexture[slot]) return;
	boundTexture[slot] = texture.texture;
	glActiveTexture(GL_TEXTURE0 + slot);
	glBindTexture(GL_TEXTURE_2D, texture.texture);
}

void Texture::unbind(int slot)
{
	if (boundTexture[slot] == 0) return;
	boundTexture[slot] = 0;
	glActiveTexture(GL_TEXTURE0 + slot);
	glBindTexture(GL_TEXTURE_2D, 0);
}