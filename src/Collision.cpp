#include "Collision.h"
#include "VoxelModel.h"
#include "game.h"
#include "VoxelModelStorage.h"
#include "TransformStorage.h"
#include "debug.h"
#include <queue>
#include <algorithm>

void Collision::createVoxelModelCollider(Entity e)
{
	addSystemToEntity(e, systemId);
	Vec3 pos = transformStorage->getTransform(e).pos;
	auto* m = vmStorage->getModelForRead(e);
	colliderBounds.emplace_back(AABB {Vec3(m->getSize() / 2) + pos, m->getSize() / 2}, e.idx);
}

std::vector<Vec3i> Collision::getBoxIntersection(AABB b)
{
	std::vector<Vec3i> res;
	for (unsigned i = 0; i < colliderBounds.size(); ++i)
	{
		AABB intersection = b.intersection(colliderBounds[i].first);
		//	bool i2 = b.intersects(colliderBounds[i]);
		if (intersection.extents.x*intersection.extents.y*intersection.extents.z < .0000001f) continue;
		Vec3 off = colliderBounds[i].first.center - colliderBounds[i].first.extents;
		intersection.center -= off;
		Vec3i mn = (intersection.center - intersection.extents);
		Vec3i mx = (intersection.center + intersection.extents);
		auto& vm = *vmStorage->getModelForRead(Entity(colliderBounds[i].second));
		auto& model = vm.getModel(0);
		for (int x = mn.x; x < mx.x; ++x)
		{
			for (int y = mn.y; y < mx.y; ++y)
			{
				for (int z = mn.z; z < mx.z; ++z)
				{
					if (model[vm.arrayPos(x, y, z)].isSolid())
					{
						res.emplace_back(x + off.x, y + off.y, z + off.z);
					}
				}
			}
		}
	}
	return res;
}

std::vector<Vec3i> Collision::getSphereIntersection(Vec3 sp, float r)
{
	AABB b = {sp, Vec3(r)};
	std::vector<Vec3i> res;
	for (unsigned i = 0; i < colliderBounds.size(); ++i)
	{
		AABB intersection = b.intersection(colliderBounds[i].first);
		//	bool i2 = b.intersects(colliderBounds[i]);
		if (intersection.extents.x*intersection.extents.y*intersection.extents.z < .0000001f) continue;
		Vec3 off = colliderBounds[i].first.center - colliderBounds[i].first.extents;
		intersection.center -= off;
		Vec3i mn = (intersection.center - intersection.extents);
		Vec3i mx = (intersection.center + intersection.extents);
		auto& vm = *vmStorage->getModelForRead(colliderBounds[i].second);
		auto& model = vm.getModel(0);
		for (int x = mn.x; x < mx.x; ++x)
		{
			for (int y = mn.y; y < mx.y; ++y)
			{
				for (int z = mn.z; z < mx.z; ++z)
				{
					if (glm::distance2(Vec3(x, y, z), sp) > r) continue;
					if (model[vm.arrayPos(x, y, z)].isSolid())
					{
						res.emplace_back(x + off.x, y + off.y, z + off.z);
					}
				}
			}
		}
	}
	return res;
}

std::vector<Entity> Collision::getBoxIntetsectionEntities(AABB b)
{
	std::vector<Entity>  res;
	for (unsigned i = 0; i < colliderBounds.size(); ++i)
	{
		Vec3 e = b.intersection(colliderBounds[i].first).extents;
		if (e.x != 0 && e.y != 0 && e.z != 0)
		{
			res.emplace_back(Entity(colliderBounds[i].second));
		}
	}
	return res;
}

void Collision::onEntityDestroy(Entity e)
{
	auto it = std::remove_if(colliderBounds.begin(), colliderBounds.end(), [e](std::pair<AABB, Entity> p) { return e.idx == p.second.idx; });
	size_t diff = colliderBounds.end() - it;
	assert(diff == 1);
	size_t size = colliderBounds.size();
	colliderBounds.resize(size - 1);
}

bool Collision::raycast(Vec3 start, Vec3 dir, Vec3 * hitPos, Entity* hitEntity)
{
	startProfiling(Raycast);
	float bestD = 99999999.99f;
	Vec3i res;
	Entity resE;
	AABB b = {start + dir*.5f, glm::abs(dir)* .5f};
	b.extents.x = max(1, b.extents.x);
	b.extents.y = max(1, b.extents.y);
	b.extents.z = max(1, b.extents.z);
	float dist = glm::length(dir);
	Vec3 normalized = dir / dist;
	for (unsigned i = 0; i < colliderBounds.size(); ++i)
	{
		AABB intersection = b.intersection(colliderBounds[i].first);
		if (intersection.extents.x*intersection.extents.y*intersection.extents.z < .0000001f) continue;
		Vec3 off = colliderBounds[i].first.center - colliderBounds[i].first.extents;
		intersection.center -= off;
		Vec3i mn = (intersection.center - intersection.extents);
		Vec3i mx = (intersection.center + intersection.extents);
		auto& vm = *vmStorage->getModelForRead(colliderBounds[i].second);
		auto& model = vm.getModel(0);
		Vec3 lp = start - off;
		for (int x = mn.x; x < mx.x; ++x)
		{
			for (int y = mn.y; y < mx.y; ++y)
			{
				for (int z = mn.z; z < mx.z; ++z)
				{
					if (model[vm.arrayPos(x, y, z)].isSolid())
					{
						Vec3 vp = Vec3(x, y, z);
						float d = glm::distance(lp, vp);
						if (d > dist || d >= bestD) continue;
						Vec3 p = lp + normalized * d;
						if (abs(p.x - vp.x) <= 2 && abs(p.y - vp.y) <= 2 && abs(p.z - vp.z) <= 2)
						{
							bestD = d;
							res = Vec3(x, y, z) + off;
							resE = colliderBounds[i].second;
						}
					}
				}
			}
		}
	}
	endProfiling(Raycast);
	if (bestD == 99999999.99f) return false;
	if (hitEntity != nullptr) *hitEntity = resE;
	if (hitPos != nullptr) *hitPos = res;
	return true;

}