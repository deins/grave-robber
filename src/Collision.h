#pragma once 
#include "common.h"
#include "math.h"
#include "System.h"
#include "Array.h"
#include "VoxelModel.h"

class Collision : public System
{
	Array<std::pair<AABB, Entity>, MAX_ENTITIES> colliderBounds;

public:
	inline Collision(SystemId id):System(id) {}
	virtual ~Collision() = default;

	void createVoxelModelCollider(Entity e);

	std::vector<Vec3i> getBoxIntersection(AABB);
	std::vector<Vec3i> getSphereIntersection(Vec3 pos, float r);
	std::vector<Entity> getBoxIntetsectionEntities(AABB);
	bool raycast(Vec3 start, Vec3 direction, Vec3 *hitPoint = nullptr, Entity* hitEntity = nullptr);

	virtual void onEntityDestroy(Entity) final override;
};