#define NOMINMAX
#include <iostream>
#include "debug.h"
#include "sdl_platform.h"
#include "game.h"
#undef main

extern void startApp();

using namespace std;

namespace
{
	SDL_Window *mainWindow;
	SDL_GLContext glContext;
	UInt64 startTime;
	UInt64 timeFrequency;

	int mainWindowWidth = 1024, mainWindowHeight = 720;
	bool mainWindowFocus;
	char keys[1024];
	char mouseButtons[16];
	int mouseX, mouseDeltaX;
	int mouseY, mouseDeltaY;

	// TODO: put these in config file
	bool windowed = true;
	bool fullscreen = false;
	bool nativeResolution = true;
	int msaa = 0;

	void init()
	{
		using namespace platform;
		if (SDL_Init(SDL_INIT_EVERYTHING ^ SDL_INIT_HAPTIC) != 0) fatalError("SDL_Init failed!");
		log("SDL_Init ok.");
		timeFrequency = SDL_GetPerformanceFrequency();
		startTime = SDL_GetPerformanceCounter();

		// Request opengl 3.2 context.
		if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE) != 0) fatalError("SDL_GL_SetAttribute fail!");
		if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3) != 0) fatalError("SDL_GL_SetAttribute fail!");
		if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2) != 0) fatalError("SDL_GL_SetAttribute fail!");

		// Buffer Sizes
		if (SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1) != 0) fatalError("SDL_GL_SetAttribute fail!");
		if (SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16) != 0) fatalError("SDL_GL_SetAttribute fail!");
		if (SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8) != 0) fatalError("SDL_GL_SetAttribute fail!");
		if (SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, msaa > 0)) fatalError("SDL_GL_SetAttribute fail!");
		if (SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, msaa)) fatalError("SDL_GL_SetAttribute fail!");

		{	// Open Window
			Uint32 flags = SDL_WINDOW_OPENGL;

			if (fullscreen)
			{
				if (windowed)
				{
					if (nativeResolution) flags |= SDL_WINDOW_BORDERLESS | SDL_WINDOW_MAXIMIZED;
					else flags |= SDL_WINDOW_RESIZABLE;
				}
				else
				{
					flags |= SDL_WINDOW_FULLSCREEN;
					if (nativeResolution)
					{
						SDL_DisplayMode dm;
						if (SDL_GetDesktopDisplayMode(0, &dm) == 0)
						{
							mainWindowWidth = dm.w;
							mainWindowHeight = dm.h;
						}
						else warn("Can't getNative resolution!!!");
					}
				}
			}
			mainWindow = SDL_CreateWindow("Game", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, mainWindowWidth, mainWindowHeight, flags);
			if (!mainWindow) fatalError("SDL_CreateWindow failed!");
			SDL_GetWindowSize(mainWindow, &mainWindowWidth, &mainWindowHeight);
			log("SDL_CreateWindow ok.");
		}

		{	// Create GL context
			glContext = SDL_GL_CreateContext(mainWindow);
			if (!glContext) fatalError("GL context creation failed!");
			if (SDL_GL_MakeCurrent(mainWindow, glContext) != 0) fatalError("GL context can't make current!");
			log("OpenGL context ok.");

			if (gl3wInit() != 0) fatalError("gl3wInit failed!");
			if (!gl3wIsSupported(3, 2)) fatalError("OpenGL 3.2 required!");
		}
	}
}

namespace platform
{
	void setTitle(const char* title)
	{
		SDL_SetWindowTitle(mainWindow, title);
	}

	std::vector<UInt8>  readAssetFile(const char* filename)
	{
		std::vector<UInt8> data;
		char fn[256];
		strcpy_s(fn, 256, "./data/");
		strcpy_s(fn + 7, 256 - 7, filename);
		SDL_RWops *f = SDL_RWFromFile(fn, "rb");
		if (f == nullptr) return data;
		size_t length = (size_t)SDL_RWseek(f, 0, RW_SEEK_END);
		if (length <= 0) return data;
		if (SDL_RWseek(f, 0, RW_SEEK_SET) == -1) return data;
		data.resize(length + 1);
		auto status = SDL_RWread(f, data.data(), 1, length);
		if (status != length) logWarning("File IO, couldn't read whole file!");
		status = SDL_RWclose(f);
		assert(status == 0);
		return data;
	}

	void display()
	{
		SDL_GL_SwapWindow(mainWindow);
	}

	void makeContextCurrent()
	{
		if (SDL_GL_MakeCurrent(mainWindow, glContext)!=0) fatalError("can't make context current");
	}

	void releaseContext()
	{
		if (SDL_GL_MakeCurrent(mainWindow, NULL) != 0) fatalError("can't release context");
	}

	void logMessage(const char* str)
	{
		SDL_Log(str);
	}

	void logWarningMessage(const char* str)
	{
		SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, str);
	}

	void logErrorMessage(const char* str)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, str);
	}

	UInt64 getMicroseconds()
	{
		return SDL_GetPerformanceCounter() * 1000000 / timeFrequency;
	}

	void showMessageBox(const char* title, const char* message, MessageBoxType type)
	{
		unsigned flags = SDL_MESSAGEBOX_INFORMATION;
		if (type == MessageBoxType::Warning) flags = SDL_MESSAGEBOX_WARNING;
		else if (type == MessageBoxType::Error) flags = SDL_MESSAGEBOX_ERROR;
		SDL_ShowSimpleMessageBox(flags, title, message, nullptr);
	}

	bool setVsync(bool state)
	{
		if (SDL_GL_SetSwapInterval(state) == 0)
			return true;

		warn("Set VSync failed!");
		return false;
	}

	void showCursor(bool state)
	{
		SDL_ShowCursor(state);
	}

	Vec2 getWindowSize()
	{
		return {mainWindowWidth, mainWindowHeight};
	}

	void processEvents()
	{
		// Input 
		mouseDeltaX = mouseDeltaY = 0;
		for (auto& k : keys)
		{
			switch (k)
			{
				case(-1) :
					// released
					k = 0;
					break;

				case(1) :
					// pressed
					k = 2;
					break;

				case(3) :
					// pressed & released in one frame
					k = 0;
					break;
			}
		}
		for (auto& k : mouseButtons)
		{
			switch (k)
			{
				case(-1) :
					// released
					k = 0;
					break;

				case(1) :
					// pressed
					k = 2;
					break;

				case(3) :
					// pressed & released in one frame
					k = 0;
					break;
			}
		}

		SDL_Event event;
		/* Grab all the events off the queue. */
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
				case SDL_QUIT:
					stop();
					break;

				case SDL_WINDOWEVENT:
					switch (event.window.event)
					{
						case (SDL_WINDOWEVENT_RESIZED) : {
							mainWindowWidth = event.window.data1;
							mainWindowHeight = event.window.data2;
						}
						case (SDL_WINDOWEVENT_FOCUS_GAINED) :
							mainWindowFocus = true;
							break;
						case (SDL_WINDOWEVENT_FOCUS_LOST) :
							mainWindowFocus = false;
							break;
					}
					break;

				case (SDL_KEYDOWN) :
					if (!event.key.repeat)
						keys[event.key.keysym.scancode] = 1;
					break;

				case (SDL_KEYUP) : {
					if (!event.key.repeat)
					{
						if (keys[event.key.keysym.scancode] == 1)
						{
							keys[event.key.keysym.scancode] = 3;
							break;
						}
						else if (keys[event.key.keysym.scancode] == 0) warn("detected key release without press!");
						keys[event.key.keysym.scancode] = -1;
					}
					break;
				}

				case (SDL_MOUSEMOTION) :
					mouseX = event.motion.x;
					mouseY = event.motion.y;
					mouseDeltaX = event.motion.xrel;
					mouseDeltaY = event.motion.yrel;
					break;

				case(SDL_MOUSEBUTTONDOWN) :
					mouseButtons[event.button.button] = 1;
					break;

				case(SDL_MOUSEBUTTONUP) :
					if (mouseButtons[event.button.button] == 1)
					{
						mouseButtons[event.button.button] = 3;
						break;
					}
					else if (mouseButtons[event.button.button] == 0) warn("detected mouse button release without press!");
					mouseButtons[event.button.button] = -1;
					break;

				default:
					//mInput.processEvent(event);
					break;
			}
		}
	}

	Key getKey(const char* name)
	{
		return (Key)SDL_GetScancodeFromName(name);
	}

	bool isKeyDown(Key k)
	{
		return keys[k] == 1 || keys[k] == 3;
	}

	bool isKeyActive(Key k)
	{
		return keys[k] > 0;
	}

	bool isKeyUp(Key k)
	{
		return keys[k] == -1;
	}

	Vec2 getMousePos() { return Vec2(mouseX, mouseY); }
	Vec2 getMouseDelta() { return Vec2(mouseDeltaX, mouseDeltaY); }
	bool isMouseButtonDown(MouseButton b) { return mouseButtons[b] == 1; }
	bool isMouseButtonUp(MouseButton b) { return mouseButtons[b] == -1; }
	bool isMouseButtonActive(MouseButton b) { return mouseButtons[b] > 0; }
}

int main(int argc, char* args)
{
	using namespace platform;
	init();
	startApp();

	return 0;
}