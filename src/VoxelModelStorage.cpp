#include "VoxelModelStorage.h"
#include "game.h"

VoxelModel* VoxelModelStorage::createInstance(VoxelModel* m)
{
	int idx = 0;
	if (freeInstances.empty())
	{
		idx = (int) instances.size();
		instances.emplace_back();
	}
	else
	{
		idx = freeInstances.back();
		freeInstances.pop_back();
	}
	instances[idx] = *m;
	return &instances[idx];
}

void VoxelModelStorage::createComponent(Entity e, VoxelModel* mdl, bool shared)
{
	addSystemToEntity(e, systemId);
	assert(mdl != nullptr);
	models[e.idx].shared = shared;
	if (shared)
		models[e.idx].model = (mdl);
	else
		models[e.idx].model = createInstance(mdl);
}

VoxelModel* VoxelModelStorage::getModelForRead(Entity e)
{
	return models[e.idx].model;
}

VoxelModel* VoxelModelStorage::getModelForWrite(Entity e)
{
	if (models[e.idx].shared)
	{
		models[e.idx].shared = 0;
		models[e.idx].model = createInstance(models[e.idx].model);
	}
	return models[e.idx].model;
}

void VoxelModelStorage::onEntityDestroy(Entity e)
{
	if (!models[e.idx].shared)
	{
		*models[e.idx].model = VoxelModel();
		freeInstances.push_back((int)(models[e.idx].model - instances.data()));
	}
	//{
	//	models[e.idx].shared = 0;
	//}

}