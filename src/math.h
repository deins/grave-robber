#pragma once
#include <cmath>
#define GLM_FORCE_PURE 
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "common.h"

const float Pi = glm::pi<float>();
const float NaN = std::nanf("");
const float Deg2Rad = Pi / 180;
const float Rad2Deg = 180 / Pi;

typedef glm::vec2 Vec2;
typedef glm::vec3 Vec3;
typedef glm::vec4 Vec4;
typedef glm::ivec2 Vec2i;
typedef glm::ivec3 Vec3i;
typedef glm::ivec4 Vec4i;
typedef glm::vec4 Colorf;

typedef glm::mat2 Mat2;
typedef glm::mat3 Mat3;
typedef glm::mat4 Mat4;
typedef glm::fquat Quaternion;

struct Transform
{
	Vec3 pos;
	Quaternion rotation;
};

struct AABB
{
	Vec3 center;
	Vec3 extents;
	bool intersects(AABB);
	AABB intersection(AABB);
};

int ceilToPow2(int);
inline int log2(int x);

//struct Vec2
//{
//	float x, y;
//	Vec2() = default;
//	Vec2(float val);
//	Vec2(float x, float y);
//	float magnitude() const;
//	float magnitude2() const;
//	Vec2 normalized() const;
//	void normalize();
//
//	void operator += (Vec2 rhs);
//	void operator -= (Vec2 rhs);
//	void operator /= (float rhs);
//	void operator *= (float rhs);
//};
//Vec2 operator + (Vec2 lhs, Vec2 rhs);
//Vec2 operator - (Vec2 lhs, Vec2 rhs);
//Vec2 operator / (Vec2 lhs, float rhs);
//Vec2 operator * (Vec2 lhs, float rhs);
//Vec2 operator / (float lhs, Vec2 rhs);
//Vec2 operator * (float lhs, Vec2 rhs);
//
//struct Vec3
//{
//	float x, y, z;
//	Vec3() = default;
//	Vec3(float val);
//	Vec3(Vec2, float z = 0);
//	Vec3(float x, float y, float z);
//	float magnitude() const;
//	float magnitude2() const;
//	Vec3 normalized() const;
//	void normalize();
//
//	void operator += (Vec3 rhs);
//	void operator -= (Vec3 rhs);
//	void operator /= (float rhs);
//	void operator *= (float rhs);
//};
//Vec3 operator + (Vec3 lhs, Vec3 rhs);
//Vec3 operator - (Vec3 lhs, Vec3 rhs);
//Vec3 operator / (Vec3 lhs, float rhs);
//Vec3 operator * (Vec3 lhs, float rhs);
//Vec3 operator / (float lhs, Vec3 rhs);
//Vec3 operator * (float lhs, Vec3 rhs);
//
//struct Vec2i
//{
//	int x, y;
//	Vec2i() = default;
//	Vec2i(int val);
//	Vec2i(int x, int y);
//	int magnitude2() const;
//	void operator += (Vec2i rhs);
//	void operator -= (Vec2i rhs);
//	void operator /= (int rhs);
//	void operator *= (int rhs);
//};
//Vec2i operator + (Vec2i lhs, Vec2i rhs);
//Vec2i operator - (Vec2i lhs, Vec2i rhs);
//Vec2i operator / (Vec2i lhs, int rhs);
//Vec2i operator * (Vec2i lhs, int rhs);
//Vec2i operator / (int lhs, Vec2i rhs);
//Vec2i operator * (int lhs, Vec2i rhs);
//
//struct Vec3i
//{
//	int x, y, z;
//	Vec3i() = default;
//	Vec3i(int val);
//	Vec3i(Vec2i val, int z);
//	Vec3i(int x, int y, int z);
//	int magnitude2() const;
//	void operator += (Vec3i rhs);
//	void operator -= (Vec3i rhs);
//	void operator /= (int rhs);
//	void operator *= (int rhs);
//};
//Vec3i operator + (Vec3i lhs, Vec3i rhs);
//Vec3i operator - (Vec3i lhs, Vec3i rhs);
//Vec3i operator / (Vec3i lhs, int rhs);
//Vec3i operator * (Vec3i lhs, int rhs);
//Vec3i operator / (int lhs, Vec3i rhs);
//Vec3i operator * (int lhs, Vec3i rhs);

template <typename T>
T clamp(T val, T minVal, T maxVal) { return (val<minVal ? minVal : (val>maxVal ? maxVal : val)); }

template <typename T>
T lerp(T a, T b, float t) { return (T)((float)a + (float)(b - a)*t); }

template <typename T>
T lerps(T a, T b, float t) { return (T)((float)a + (float)(b - a)*clamp(t, 0.0f, 1.0f)); } // safe lerp (clamps t)

template <typename T>
T move(T a, T b, float speed)
{
	auto dir = b - a;
	auto len = abs(dir);
	return (T)(a + dir / len*speed);
}

template<> Vec2 move<Vec2>(Vec2 a, Vec2 b, float speed);
template<> Vec3 move<Vec3>(Vec3 a, Vec3 b, float speed);