#pragma once 
#include "common.h"
#include "math.h"
#include "Color.h"
#include "System.h"
#include "platform.h"
#include "Array.h"
#include <vector>
#include <atomic>

class VoxelModel;

struct VoxelPoint
{
	Vec3 pos;
	Color col;
};

struct Camera
{
	Transform transform;
	float fov = 80;
	float far = 1000;
	float near = .1f;
};

class Renderer : public System
{
	class VBO
	{
	public:
		enum Mode
		{
			Uninitialized = 0,
			DynamicDraw = GL_DYNAMIC_DRAW,
			StaticDraw = GL_STATIC_DRAW,
			StreamDraw = GL_STREAM_DRAW,

			DynamicCopy = GL_DYNAMIC_COPY,
			StaticCopy = GL_STATIC_COPY,
			StreamCopy = GL_STREAM_COPY,

			DynamicRead = GL_DYNAMIC_READ,
			StaticRead = GL_STATIC_READ,
			StreamRead = GL_STREAM_READ
		};
	private:
		static GLuint boundVBO;
		GLuint vbo;
		Mode mode;
		int size;
	public:
		VBO();
		VBO(VBO&) = delete;
		VBO operator = (VBO&) = delete;
		~VBO();
		void bind();
		void uploadVertices(const std::vector<VoxelPoint>& data, Mode mode);
		static void bind(VBO& vbo);
		static void unbind();
		GLuint getValue();
		int getSize();
	};

	struct VoxelModelRenderer
	{
		AABB bounds;
		Array<VBO, 16> vbos;
		Array<size_t, 16> vbosSize;
		Entity owner;
		bool isStatic;
	};

	struct VoxelModelRenderData
	{
		Mat4 transform;
		GLuint vbo;
		unsigned voxels;
	};


	VoxelModelRenderer voxelModels[MAX_ENTITIES];
	Array<VoxelModelRenderData, MAX_ENTITIES> bilboardVoxelVbos[16];
	Array<VoxelModelRenderData, MAX_ENTITIES> voxelVbos[16];

	enum ThreadStatus
	{
		Waiting,
		Working,
		Terminate
	};
	std::atomic<ThreadStatus> threadStatus;

	Camera cam, camCopy;
	Mat4 view, projection, vp;
	float lodQuality;
public:
	static void checkOpenGLContext();

	inline Renderer(SystemId idx): System(idx) {};
	virtual ~Renderer() = default;

	virtual void init() override;
	virtual void onEntityDestroy(Entity) final override;

	void renderThread();
	void createVoxelModel(Entity e,bool isStatic = true);
	void updateVoxelModel(Entity);
	void render();
	//void renderVoxelModel(VoxelModel*, Transform);

	Camera& getCamera();
};