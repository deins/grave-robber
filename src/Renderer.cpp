#include "Renderer.h"
#include "game.h"
#include "Material.h"
#include "Color.h"
#include "platform.h"
#include "debug.h"
#include "math.h"
#include "VoxelModel.h"
#include "Frustum.h"
#include "VoxelModelStorage.h"
#include "TransformStorage.h"
#include <stb_truetype.h>
#include <algorithm>

#include <vector>

using namespace std;
using namespace glm;

const char* glErrorString(GLenum err)
{
	const char* str = nullptr;
	switch (err)
	{
		case GL_NO_ERROR: break;
		case GL_INVALID_OPERATION:
			str = "INVALID_OPERATION";
			break;
		case GL_INVALID_ENUM:
			str = "INVALID_ENUM";
			break;
		case GL_INVALID_VALUE:
			str = "INVALID_VALUE";
			break;
		case GL_OUT_OF_MEMORY:
			str = "OUT_OF_MEMORY";
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			str = "INVALID_FRAMEBUFFER_OPERATION";
			break;
		default:
			str = "UNKNOWN_GL_ERROR";
			break;
	}
	return str;
}

// VBO
GLuint Renderer::VBO::boundVBO = 0;
Renderer::VBO::VBO()
{
	checkOpenGLContext();
	glGenBuffers(1, &vbo);
	mode = Mode::Uninitialized;
	size = 0;
}

Renderer::VBO::~VBO()
{
	checkOpenGLContext();
	if (boundVBO == vbo)
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		boundVBO = 0;
	}
	glDeleteBuffers(1, &vbo);
}

int Renderer::VBO::getSize() { return size; }

void Renderer::VBO::bind() { bind(*this); }

void Renderer::VBO::uploadVertices(const std::vector<VoxelPoint>& data, Mode mode)
{
	this->mode = mode;
	size = (int)data.size();
	bind();
	glDebugCheck();
	glBufferData(GL_ARRAY_BUFFER, sizeof(VoxelPoint) * data.size(), data.data(), mode);
	glDebugCheck();
}

void Renderer::VBO::bind(VBO& vbo)
{
	if (boundVBO != vbo.vbo)
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbo.vbo);
		boundVBO = vbo.vbo;
	}
}

void Renderer::VBO::unbind()
{
	if (boundVBO != 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		boundVBO = 0;
	}
}

GLuint Renderer::VBO::getValue()
{
	return this->vbo;
}

void Renderer::checkOpenGLContext() { /* TODO: implement this */ }

void Renderer::init()
{
	threadStatus = Waiting;
	cam.transform.pos = Vec3(0, 0, 4);
	checkOpenGLContext();
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	//glViewport(0, 0, 128, 128);
	GLuint VoxelPointArrayId;
	glGenVertexArrays(1, &VoxelPointArrayId);
	glBindVertexArray(VoxelPointArrayId);
	glDebugCheck();

	lodQuality = getConfigParameter<float>("graphics/lod_distance"); // 1 ,2 ,4, 8 recommended
	if (lodQuality <= 0.0) fatalError("config invalid lod_distance, allowed only floats greater than 0!");
}

void Renderer::renderThread()
{
	platform::makeContextCurrent();
	while (threadStatus != Terminate)
	{
		while (threadStatus == Waiting);

		// drawing goes here

		threadStatus = Waiting;
		platform::display();
	}
	platform::releaseContext();
}

void Renderer::updateVoxelModel(Entity e)
{
	auto& vmr = this->voxelModels[e.idx];
	auto *model = vmStorage->getModelForRead(e);
	Vec3 off = vmr.bounds.center - vmr.bounds.extents;
	int lods = model->getMaxLod();
	if (vmr.vbos.size() != lods + 1)
		vmr.vbos.resize(lods + 1);
	for (int i = 0; i <= lods; i++)
	{
		auto vps = model->getVisible(i);
		if (vmr.isStatic) for (auto& v : vps)	v.pos += off;
		vmr.vbosSize[i] = vps.size();
		vmr.vbos[i].uploadVertices(vps, VBO::Mode::DynamicDraw);
	}
}


void Renderer::render()
{
	//while (threadStatus == Working);
	// Camera
	float width = platform::getWindowSize().x, height = platform::getWindowSize().y;
	view = glm::mat4_cast(inverse(cam.transform.rotation)) * glm::translate(Mat4(), -cam.transform.pos);
	projection = glm::perspectiveFov(cam.fov * Deg2Rad, width, height, cam.near, cam.far);
	vp = projection * view;

	// Filter static voxel models
	for (int i = 0; i < 16; ++i) bilboardVoxelVbos[i].clear();
	for (int i = 0; i < 16; ++i) voxelVbos[i].clear();
	Frustum frustum(vp);
	for (auto& vmr : voxelModels)
	{
		Mat4 m;
		if (!vmr.isStatic)
		{
			Transform& t = transformStorage->getTransform(vmr.owner);
			if (vmr.vbosSize.size() == 0 || vmr.vbosSize[0] == 0 ||
				!frustum.isBoxInFrustum(vmr.bounds.center + t.pos, vmr.bounds.extents)) continue;

			m = glm::translate(m, t.pos);

			// LOD
			const float lodf = logf(glm::distance(cam.transform.pos, vmr.bounds.center + t.pos) / (64 * lodQuality));
			unsigned lod = (unsigned)max(0.0f, lodf);
			lod = min((unsigned)vmr.vbos.size() - 1, lod);
			if (vmr.vbosSize[lod] == 0) continue;
			bool useBilboards = lodf > .4 - lodQuality*.05f;

			if (useBilboards) bilboardVoxelVbos[lod].emplace_back(VoxelModelRenderData {m, vmr.vbos[lod].getValue(), vmr.vbos[lod].getSize()});
			else voxelVbos[lod].emplace_back(VoxelModelRenderData {m, vmr.vbos[lod].getValue(), vmr.vbos[lod].getSize()});
		}
		else
		{
			if (vmr.vbosSize.size() == 0 || vmr.vbosSize[0] == 0 || !frustum.isBoxInFrustum(vmr.bounds)) continue;
			// LOD
			const float lodf = logf(glm::distance(cam.transform.pos, vmr.bounds.center) / (64 * lodQuality));
			unsigned lod = (unsigned)max(0.0f, lodf);
			lod = min((unsigned)vmr.vbos.size() - 1, lod);
			if (vmr.vbosSize[lod] == 0) continue;
			bool useBilboards = lodf > .4 - lodQuality*.05f;

			if (useBilboards) bilboardVoxelVbos[lod].emplace_back(VoxelModelRenderData {Mat4(), vmr.vbos[lod].getValue(), vmr.vbos[lod].getSize()});
			else voxelVbos[lod].emplace_back(VoxelModelRenderData {Mat4(), vmr.vbos[lod].getValue(), vmr.vbos[lod].getSize()});
		}
	}
	//threadStatus = Working;

	{	// This could be done in other thread
		Color clearColor = Color(0, 0, 64);
		glClearColor(clearColor.getRedf(), clearColor.getGreenf(), clearColor.getBluef(), clearColor.getAlphaf());
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDebugCheck();

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		{
			Material& material = *getMaterial("low_voxels");
			material.trySetFloat("Frame", (float)getCurrentFrame());
			material.trySetMat4("View", view);
			material.trySetMat4("Projection", projection);
			material.trySetVec3("CamPos", cam.transform.pos);
			material.bind();
			for (int lod = 0; lod < 16; ++lod)
			{
				if (bilboardVoxelVbos[lod].empty()) continue;
				material.setFloat("Size", float(1 << lod)*1.9f);
				for (auto& rd : bilboardVoxelVbos[lod])
				{
					material.trySetMat4("Model", rd.transform);
					material.trySetMat4("MVP", vp * rd.transform);
					glBindBuffer(GL_ARRAY_BUFFER, rd.vbo);
					glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VoxelPoint), (void*)0);
					glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VoxelPoint), (void*)sizeof(Vec3));
					glDrawArrays(GL_POINTS, 0, rd.voxels);
				}
			}
		}

		{
			Material& material = *getMaterial("voxels");
			material.trySetFloat("Frame", (float)getCurrentFrame());
			material.trySetMat4("View", view);
			material.trySetMat4("Projection", projection);
			material.trySetVec3("CamPos", cam.transform.pos);
			material.bind();
			for (int lod = 0; lod < 16 && !voxelVbos[lod].empty(); ++lod)
			{
				if (voxelVbos[lod].empty()) continue;
				material.setFloat("Size", float(1 << lod));
				for (auto& rd : voxelVbos[lod])
				{
					material.trySetMat4("Model", rd.transform);
					material.trySetMat4("MVP", vp * rd.transform);
					glBindBuffer(GL_ARRAY_BUFFER, rd.vbo);
					glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VoxelPoint), (void*)0);
					glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VoxelPoint), (void*)sizeof(Vec3));
					glDrawArrays(GL_POINTS, 0, rd.voxels);
				}
				voxelVbos[lod].clear();
			}
		}

		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(0);
		glDebugCheck();
		platform::display();
	}
}

Camera& Renderer::getCamera()
{
	return cam;
}

void Renderer::createVoxelModel(Entity e, bool s)
{
	addSystemToEntity(e, this->systemId);
	Vec3 pos = transformStorage->getTransform(e).pos;
	VoxelModel* model = vmStorage->getModelForRead(e);
	assert(model != nullptr);
	VoxelModelRenderer& vmr = voxelModels[e.idx];
	vmr.isStatic = s;
	vmr.bounds.extents = model->getSize(0) / 2;
	vmr.bounds.center = vmr.bounds.extents;
	if (vmr.isStatic)  vmr.bounds.center += pos;
	vmr.owner = e;
	for (unsigned i = 0; i <= model->getMaxLod(); i++)
	{
		auto vps = model->getVisible(i);
		if (vmr.isStatic)
		{
			for (auto& v : vps)	v.pos += pos;
		}
		vmr.vbos.emplace_back();
		vmr.vbosSize.emplace_back(vps.size());
		vmr.vbos.back().uploadVertices(vps, VBO::Mode::DynamicDraw);
	}
}

void Renderer::onEntityDestroy(Entity e)
{
	voxelModels[e.idx].vbosSize.resize(0);
}

//void Renderer::renderVoxelModel(Entity e, Transform t)
//{
//	VoxelModel* model = vmStorage->readModel(e);
//	assert(model != nullptr);
//	voxelModelRenderers.emplace_back();
//	voxelModelRenderers.back().model = model;
//	voxelModelRenderers.back().transform = t;
//}
