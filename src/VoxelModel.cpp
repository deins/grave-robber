#include "VoxelModel.h"
#include "Image.h"
#include "debug.h"

VoxelModel::VoxelModel()
{
	memset(lodValid, 0, sizeof(lodValid));
	memset(visibleValid, 0, sizeof(visibleValid));
}

VoxelModel::VoxelModel(Vec3i s, std::vector<VoxelData> m)
{
	model[0] = m;
	size.emplace_back(s);
	while (size.back() != Vec3i(1))
	{
		size.emplace_back(size.back() / 2);
		if (size.back().x < 1) size.back().x = 1;
		if (size.back().y < 1) size.back().y = 1;
		if (size.back().z < 1) size.back().z = 1;
	}
}

inline int VoxelModel::arrayPos(int x, int y, int z, unsigned lod) const
{
	return arrayPos(x, y, z, size[lod]);
}

inline int VoxelModel::arrayPos(int x, int y, int z, Vec3i size) const
{
	return x + y*size.x + z*size.x*size.y;
}

void VoxelModel::create(Vec3i s, Color color)
{
	assert(size.size() == 0);
	model[0].resize(s.x*s.y*s.z, color);
	size.emplace_back(s);
	while (size.back() != Vec3i(1))
	{
		size.emplace_back(size.back() / 2);
		if (size.back().x < 1) size.back().x = 1;
		if (size.back().y < 1) size.back().y = 1;
		if (size.back().z < 1) size.back().z = 1;
	}
}

void VoxelModel::createAndCarve(const Image* front, const Image* left, const Image* top, Color color)
{
	Vec3i size = Vec3i(1);
	if (front != nullptr)
	{
		size.x = front->getWidth();
		size.y = front->getHeight();
	}
	if (top != nullptr)
	{
		size.y = max(size.y, top->getHeight());
		size.z = max(size.z, top->getWidth());
	}
	if (left != nullptr)
	{
		size.z = max(size.z, left->getWidth());
		size.y = max(size.y, left->getHeight());
	}
	create(size, color);
	carve(front, left, top);
}

void VoxelModel::carve(const Image* front, const Image* left, const Image* top)
{
	const Vec3i size = this->size[0];
	// Front
	if (front != nullptr)
	{
		for (int x = 0; x < size.x; ++x)
		{
			for (int y = 0; y < size.y; y++)
			{
				Color col = front->getPixel(x, size.y - y - 1);
				if (col.a < 4)
				{
					for (int z = 0; z < size.z; ++z)
					{
						model[0][arrayPos(x, y, z)].erase();
					}
				}
			}
		}
	}
	// top
	if (top != nullptr)
	{
		for (int x = 0; x < size.x; ++x)
		{
			for (int z = 0; z < size.z; ++z)
			{
				Color col = top->getPixel(x, size.z - z - 1);
				if (col.a < 4)
				{
					for (int y = 0; y < size.y; y++)
					{
						model[0][arrayPos(x, y, z)].erase();
					}
				}
			}
		}
	}
	// left
	if (left != nullptr)
	{
		for (int z = 0; z < left->getWidth(); ++z)
		{
			for (int y = 0; y < left->getHeight(); y++)
			{
				Color col = left->getPixel(z, left->getHeight() - y - 1);
				if (col.a < 4)
				{
					for (int x = 0; x < size.x; x++)
					{
						model[0][arrayPos(x, y, z)].erase();
					}
				}
			}
		}
	}
}

void VoxelModel::paint(const Image* front, const Image* left, const Image* top)
{
	const Vec3i size = this->size[0];
	// Single sided
	if ((!front && !left) || (!left && !top) || (!front && !top))
	{
		// Front
		if (front != nullptr)
		{
			for (int x = 0; x < size.x; ++x)
			{
				for (int y = 0; y < size.y; y++)
				{
					for (int z = 0; z < size.z; ++z)
						if (model[0][arrayPos(x, y, z)].isSolid())
							model[0][arrayPos(x, y, z)].setColor(front->getPixel(x, z));
				}
			}
		}
		// left
		if (left != nullptr)
		{
			int w = min(size.z, left->getWidth());
			int h = min(size.y, left->getHeight());
			for (int z = 0; z < w; ++z)
			{
				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < size.x; x++)
					{
						if (model[0][arrayPos(x, y, z)].isSolid())
							model[0][arrayPos(x, y, z)].setColor(left->getPixel(x, z));
					}
				}
			}
		}

		// top
		if (top != nullptr)
		{
			int w = min(size.x, top->getWidth());
			int h = min(size.z, top->getHeight());
			for (int x = 0; x < w; x++)
			{
				for (int z = 0; z < h; ++z)
				{
					for (int y = 0; y < size.y; y++)
					{
						if (model[0][arrayPos(x, y, z)].isSolid())
							model[0][arrayPos(x, y, z)].setColor(top->getPixel(x, z));
					}
				}
			}
		}
		return;
	}

	std::vector<int> hm[6]; // height maps: front,back,left,right,top, bottom
	hm[0].resize(size.x*size.y);
	hm[1].resize(size.x*size.y, size.z - 1);
	hm[2].resize(size.z*size.y);
	hm[3].resize(size.z*size.y, size.x - 1);
	hm[4].resize(size.x*size.z, size.y - 1);
	hm[5].resize(size.x*size.z);
	{// Calculate absolute height maps
		// Front
		if (front != nullptr)
		{
			for (int x = 0; x < size.x; ++x)
			{
				for (int y = 0; y < size.y; y++)
				{
					for (int z = 0; z < size.z; ++z)
					{
						if (model[0][arrayPos(x, y, z)].isSolid())
						{
							hm[0][x + size.x*y] = z;
							break;
						}
					}
					for (int z = size.z - 1; z >= 0; --z)
					{
						if (model[0][arrayPos(x, y, z)].isSolid())
						{
							hm[1][x + size.x*y] = z;
							break;
						}
					}
				}
			}
		}
		// left
		if (left != nullptr)
		{
			int w = min(size.z, left->getWidth());
			int h = min(size.y, left->getHeight());
			for (int z = 0; z < w; ++z)
			{
				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < size.x; x++)
					{
						if (model[0][arrayPos(x, y, z)].isSolid())
						{
							hm[2][z + size.z*y] = x;
							break;
						}
					}
					for (int x = size.x - 1; x >= 0; --x)
					{
						if (model[0][arrayPos(x, y, z)].isSolid())
						{
							hm[3][z + size.z*y] = x;
							break;
						}
					}
				}
			}
		}

		// top
		if (top != nullptr)
		{
			int w = min(size.x, top->getWidth());
			int h = min(size.z, top->getHeight());
			for (int x = 0; x < w; x++)
			{
				for (int z = 0; z < h; ++z)
				{
					for (int y = size.y - 1; y >= 0; y--)
					{
						if (model[0][arrayPos(x, y, z)].isSolid())
						{
							/*		if (top)
										model[0][arrayPos(x, y, z)].setColor(top->getPixel(x, z));
										else*/
							{
								hm[4][x + size.x*z] = y;
								break;
							}
						}
					}
					for (int y = 0; y < size.y; y++)
					{
						if (model[0][arrayPos(x, y, z)].isSolid())
						{
				/*			if (top)
								model[0][arrayPos(x, y, z)].setColor(top->getPixel(x, z));
							else*/
							{
								hm[5][x + size.x*z] = y;
								break;
							}
						}
					}
				}
			}
		}
	}

	// Paint
	if (left && front)
	{
		int sy = min(size.y, min(front->getHeight(), left->getHeight()));
		for (int x = 0; x < size.x; ++x)
		{
			for (int y = 0; y < sy; y++)
			{
				for (int z = 0; z < size.z; ++z)
				{
					if (model[0][arrayPos(x, y, z)].isSolid())
					{
						Colorf fc = front->getPixel(x, size.y - y - 1).getColorf();
						Colorf lc = left->getPixel(z, size.y - y - 1).getColorf();
						float ld = min((x - hm[2][z + size.z*y]), (hm[3][z + size.z*y] - x)) / float(hm[3][z + size.z*y] - hm[2][z + size.z*y]) * 2;
						float fd = min((z - hm[0][x + size.x*y]), (hm[1][x + size.x*y] - z)) / float(hm[1][x + size.x*y] - hm[0][x + size.x*y]) * 2;
						if (ld <= fd)
							model[0][arrayPos(x, y, z)].setColor(glm::lerp(lc, fc, min(.5f, ld)));
						else
							model[0][arrayPos(x, y, z)].setColor(glm::lerp(fc, lc, min(.5f, fd)));
					}
				}
			}
		}
	}

	if (left && front && top)
	{
		int sy = min(size.y, min(front->getHeight(), left->getHeight()));
		for (int x = 0; x < size.x; ++x)
		{
			for (int y = 0; y < sy; y++)
			{
				for (int z = 0; z < size.z; ++z)
				{
					if (model[0][arrayPos(x, y, z)].isSolid())
					{
						Colorf fc = front->getPixel(x, size.y - y - 1).getColorf();
						Colorf lc = left->getPixel(z, size.y - y - 1).getColorf();
						Colorf tc = top->getPixel(x, size.z - z - 1).getColorf();
						float ld = min((x - hm[2][z + size.z*y]), (hm[3][z + size.z*y] - x)) / float(hm[3][z + size.z*y] - hm[2][z + size.z*y]) * 2;
						float fd = min((z - hm[0][x + size.x*y]), (hm[1][x + size.x*y] - z)) / float(hm[1][x + size.x*y] - hm[0][x + size.x*y]) * 2;
						float td = min((y - hm[5][x + size.x*z]), (hm[4][x + size.x*z] - y)) / float(hm[4][x + size.x*z] - hm[5][x + size.x*z]) * 2;

						if (ld <= fd)
						{
							fc = glm::lerp(lc, fc, min(.5f, ld));
							if (td < ld) fc = glm::lerp(tc, fc, td);
						}
						else
						{
							fc = glm::lerp(fc, lc, min(.5f, fd));
							if (td < fd) fc = glm::lerp(tc, fc, td);
						}
						model[0][arrayPos(x, y, z)].setColor(fc);
					}
				}
			}
		}
	}
}

void VoxelModel::generateLod(unsigned rlod)
{
	assert(!lodValid[rlod]);
	startProfiling(VoxelModel_generateLods);
	for (unsigned lod = 1; lod <= rlod; ++lod)
	{
		if (lodValid[lod]) continue;
		else lodValid[lod] = 1;
		const int plod = lod - 1;
		Vec3i psize = this->size[plod];
		Vec3i& nsize = size[lod];

		model[lod].clear();
		model[lod].resize(nsize.x*nsize.y*nsize.z);

		for (int x = 0; x < nsize.x; x++)
		{
			for (int y = 0; y < nsize.y; y++)
			{
				for (int z = 0; z < nsize.z; z++)
				{
					const Vec3i offsets[8] = {
						Vec3i(0, 0, 0), Vec3i(1, 0, 0), Vec3i(0, 1, 0), Vec3i(1, 1, 0),
						Vec3i(0, 0, 1), Vec3i(1, 0, 1), Vec3i(0, 1, 1), Vec3i(1, 1, 1)
					};
					int q = 0;
					Colorf c = Colorf(0);
					for (int i = 0; i < 8; ++i)
					{
						Vec3i p = Vec3i(x, y, z) * 2 + offsets[i];
						if (p.x >= psize.x || p.y >= psize.y || p.z >= psize.z) continue;
						//assert(!(p.x >= psize.x || p.y >= psize.y || p.z >= psize.z));
						if (model[plod][arrayPos(p.x, p.y, p.z, plod)].isSolid())
						{
							++q;
							c += model[plod][arrayPos(p.x, p.y, p.z, plod)].getColorf();
						}
					}
					if (q>0) model[lod][arrayPos(x, y, z, lod)] = VoxelData(c / q);
				}
			}
		}
	}
	endProfiling(VoxelModel_generateLods);
}

std::vector<VoxelPoint>& VoxelModel::getVisible(unsigned lod)
{
	if (!lodValid[lod]) generateLod(lod);
	if (!visibleValid[lod])
	{
		visibleValid[lod] = true;
		std::vector<VoxelPoint>& visible = this->visible[lod];
		visible.clear();
		// Find visible
		int q = 0;
		auto size = this->size[lod];
		int lodSize = 1 << lod;
		for (int x = 0; x < size.x; x++)
		{
			for (int y = 0; y < size.y; y++)
			{
				for (int z = 0; z < size.z; z++)
				{
					if (model[lod][arrayPos(x, y, z, lod)].isEmpty()) continue;
					UInt8 f = 0;
					f |= UInt8(x == size.x - 1 || model[lod][arrayPos(x + 1, y, z, size)].isEmpty()) << 2; // right
					f |= UInt8(x == 0 || model[lod][arrayPos(x - 1, y, z, lod)].isEmpty()) << 3; // left

					f |= UInt8(y == size.y - 1 || model[lod][arrayPos(x, y + 1, z, size)].isEmpty()) << 4; // Top
					f |= UInt8(y == 0 || model[lod][arrayPos(x, y - 1, z, lod)].isEmpty()) << 5; // Bottom

					f |= UInt8(z == size.z - 1 || model[lod][arrayPos(x, y, z + 1, size)].isEmpty()); // front
					f |= UInt8(z == 0 || model[lod][arrayPos(x, y, z - 1, lod)].isEmpty()) << 1; // back

					model[lod][arrayPos(x, y, z, size)].setVisibleFaces(f);
					if (model[lod][arrayPos(x, y, z, size)].isVisible())
					{
						q++;
						visible.emplace_back(VoxelPoint {Vec3(x*lodSize, y*lodSize, z*lodSize), model[lod][arrayPos(x, y, z, size)].getColor()});
					}
				}

			}
		}
		log("lod %d has %d visible voxels", lod, this->visible[lod].size());
	}

	return this->visible[lod];
}

unsigned VoxelModel::getMaxLod()
{
	return (unsigned)size.size() - 1;
}

Vec3i VoxelModel::getSize(unsigned lod)
{
	assert(lod >= 0 && size.size() > lod);
	return size[lod];
}

void VoxelModel::destoySphere(Vec3i pos, int r)
{
	r *= r;
	auto size = this->size[0];
	Vec3i mn = Vec3(max(0, pos.x - r), max(0, pos.y - r), max(0, pos.z - r));
	Vec3i mx = Vec3(min(size.x, pos.x + r + 1), min(size.y, pos.y + r + 1), min(size.z, pos.z + r + 1));
	for (int x = mn.x; x < mx.x; x++)
	{
		for (int y = mn.y; y < mx.y; y++)
		{
			for (int z = mn.z; z < mx.z; z++)
			{
				if ((pos.x - x)*(pos.x - x) + (pos.y - y)*(pos.y - y) + (pos.z - z)*(pos.z - z) <= r)
					model[0][arrayPos(x, y, z)].erase();
			}
		}
	}
	invalideLods();
}

std::vector<VoxelPoint> VoxelModel::destoyAndGetSphere(Vec3i pos, int r)
{
	std::vector<VoxelPoint> res;
	r *= r;
	auto size = this->size[0];
	Vec3i mn = Vec3(max(0, pos.x - r), max(0, pos.y - r), max(0, pos.z - r));
	Vec3i mx = Vec3(min(size.x, pos.x + r + 1), min(size.y, pos.y + r + 1), min(size.z, pos.z + r + 1));
	for (int x = mn.x; x < mx.x; x++)
	{
		for (int y = mn.y; y < mx.y; y++)
		{
			for (int z = mn.z; z < mx.z; z++)
			{
				if ((pos.x - x)*(pos.x - x) + (pos.y - y)*(pos.y - y) + (pos.z - z)*(pos.z - z) <= r)
				{
					res.emplace_back(VoxelPoint {Vec3(x, y, z), model[0][arrayPos(x, y, z)].getColor()});
					model[0][arrayPos(x, y, z)].erase();
				}
			}
		}
	}
	invalideLods();
	return res;
}

void VoxelModel::invalideLods()
{
	memset(visibleValid, 0, sizeof(visibleValid));
	memset(lodValid, 0, sizeof(lodValid));
}

std::vector<VoxelData>& VoxelModel::getModel(int lod)
{
	return model[lod];
}