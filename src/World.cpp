#include "World.h"
#include "Renderer.h"
#include "VoxelModel.h"
#include "game.h"
#include "VoxelModelStorage.h"
#include "TransformStorage.h"
#include "Collision.h"
#include <algorithm>

//std::vector<Entity>* World::getCell(int x, int y)
//{
//	auto it = grid.find(UInt64(x) | (UInt64(y) << 32LL));
//	if (it == grid.end()) return nullptr;
//	else return &it->second;
//}
//
//std::vector<Entity>& World::getOrCreateCell(int x, int y)
//{
//	return grid.emplace(UInt64(x) | (UInt64(y) << 32LL),std::vector<Entity>()).first->second;
//}

void World::init()
{
	renderer->getCamera().transform.pos = Vec3(48, 64, 64);
	renderer->getCamera().near = 1;
	renderer->getCamera().far = 10000;

	VoxelModel& model = *getVoxelModel("house2");
	VoxelModel& model2 = *getVoxelModel("house");
	Transform t;
	VoxelModel* grass = getVoxelModel("grass");
	VoxelModel* dirt = getVoxelModel("dirt");
	for (int i = 0; i < 32; ++i)
		for (int j = 0; j < 32; ++j)
		{
			t.pos.x = i * 32.0f;
			t.pos.z = j * 32.0f;
			t.pos.y = -2;
			Entity e = createEntity("Grass");
			transformStorage->getTransform(e) = t;
			vmStorage->createComponent(e, grass);
			renderer->createVoxelModel(e);
			collision->createVoxelModelCollider(e);
		}

	for (int i = 0; i < 32; ++i)
		for (int j = 0; j < 32; ++j)
		{
			t.pos.x = i * 64.0f;
			t.pos.z = j * 64.0f;
			t.pos.y = -66;
			Entity e = createEntity("Dirt");
			transformStorage->getTransform(e) = t;
			vmStorage->createComponent(e, dirt);
			renderer->createVoxelModel(e);
			collision->createVoxelModelCollider(e);
		}

	t = Transform();
	mouseSensitivity.x = getConfigParameter<float>("input/mouse_sensitivity/x");
	mouseSensitivity.y = getConfigParameter<float>("input/mouse_sensitivity/y");
	//Entity e = createEntity("house");
	//transformStorage->getTransform(e).pos = Vec3(155, 0, 155);
	//vmStorage->createComponent(e,getVoxelModel("house2"));
	//renderer->createVoxelModel(e);
	//collision->createVoxelModelCollider(e);
}

void World::update(float dt)
{
	static bool editor = 0;
	if (platform::isKeyDown(platform::getKey("insert"))) editor = !editor;
	{// Camera

		float lx = platform::getMouseDelta().x;
		float ly = platform::getMouseDelta().y;
		lx *= platform::isMouseButtonActive(MouseButton::Right);
		ly *= platform::isMouseButtonActive(MouseButton::Right);
		lx *= -mouseSensitivity.x;
		ly *= -mouseSensitivity.y;
		camAngles.y += lx;
		camAngles.x += ly;
		float limit = glm::radians(90.0f);
		if (camAngles.x > limit) camAngles.x = limit;
		else if (camAngles.x < -limit) camAngles.x = -limit;
		Transform& ct = renderer->getCamera().transform;
		ct.rotation = Quaternion(camAngles);//Quaternion(Vec3(0.0f, lx, 0.0f)) *  ct.rotation * Quaternion(Vec3(ly, 0.0f, 0.0f));
		//Vec3 angles = glm::eulerAngles(ct.rotation);
		//if (angles.y > limit) angles.z = limit;
		//if (angles.y < -limit) angles.z = -limit;
		//ct.rotation = Quaternion(angles);

		int f = (platform::isKeyActive(platform::getKey("s")) - platform::isKeyActive(platform::getKey("w")));
		int s = (platform::isKeyActive(platform::getKey("d")) - platform::isKeyActive(platform::getKey("a")));
		if (platform::isKeyActive(platform::getKey("Left Shift"))) f *= 10, s *= 10;
		Vec3 dp = ct.rotation * (Vec3(s, 0, f) * 60);
		if (!editor) dp.y = 0;
		ct.pos += dp * dt;

		if (!editor)
		{
			camVelocity.y -= dt * 300;
			if (camVelocity.y < -300) camVelocity.y = -300;
			{
				// Collision Y
				Vec3i cpi = ct.pos;
				Vec3 np = ct.pos;
				np.y += camVelocity.y * dt;
				if (camVelocity.y < 0)
				{
					auto colp = collision->getBoxIntersection({cpi - Vec3i(0, 10, 0), Vec3i(2, 6, 2)});
					if (colp.size() > 2)
					{
						if (platform::isKeyDown(platform::getKey("space"))) camVelocity.y = 100;
						else camVelocity.y = 0;
						Int32 hmap[5][5];
						memset(hmap, 0xFE, sizeof(hmap));
						for (auto p : colp)
						{
							p.x -= (int)cpi.x - 2;
							p.z -= (int)cpi.z - 2;
							if (p.x >= 0 && p.x < 5 && p.z >= 0 && p.z < 5)
								hmap[p.x][p.z] = max(hmap[p.x][p.z], p.y);
							//else happens rarely
						}

						np.y -= 16;
						int q = 1;
						std::sort((Int32*)hmap, (Int32*)&hmap[5][0]);
						int i = 0;
						for (; i < 25; ++i)
						{
							int v = ((Int32*)hmap)[i];
							if (v >	np.y)
								break;
						}
						for (i += 5; i < 20; ++i)
						{
							int v = ((Int32*)hmap)[i];
							np.y += v;
							++q;
						}
						if (q >= 1)
						{
							np.y /= q;
							np.y += 16;
							np.y = ct.pos.y*.7f + np.y*.3f;
						}
						else fatalError("wut collision!");
					}
				}

				{	// Wall collision
					const float r = 8;
					auto colp = collision->getBoxIntersection({np - Vec3(0, 3, 0), Vec3i(r, 8, r)});
					float xx = 0;
					float xm = 0;
					float zx = 0;
					float zm = 0;
					int my = -99999999;
					for (auto p : colp)
					{
						auto ds = glm::distance2(Vec2((float)p.x, (float)p.z), Vec2(np.x, np.z));
						if (ds > r * r) continue;
						if (ds < 3 * 3) my = max(p.y, my);
						if (abs(p.x - np.x) > abs(p.z - np.z))
						{
							if (p.x < np.x)
								xx = max(xx, p.x - np.x + r);
							else
								xm = min(xm, p.x - np.x - r + 1);
						}
						if (abs(p.x - np.x) < abs(p.z - np.z))
						{
							if (p.z < np.z)
								zx = max(zx, p.z - np.z + r);
							else
								zm = min(zm, p.z - np.z - r + 1);
						}
					}
					if (my>np.y + 1 && camVelocity.y > 0)
					{
						np = camOldPos;
						camVelocity.y = .01f;
					}
					else
					{
						if (xx == 0 || xm == 0)
							np.x += xx + xm;

						if (zx == 0 || zm == 0)
							np.z += zx + zm;
					}
				}
				camOldPos.x = ct.pos.x;
				camOldPos.z = ct.pos.z;
				ct.pos = glm::lerp(ct.pos, np, .9f);
			}
		}
		else
		{
			camVelocity = Vec3(0);
			// Editor
			if (platform::isKeyDown(platform::getKey("1")))
			{
				if (isEntityValid(editEnt))
				{
					destroyEntity(editEnt);
				}
				editEnt = createEntity("EditorEntity");
				vmStorage->createComponent(editEnt, getVoxelModel("house"));
				Vec3& p = transformStorage->getTransform(editEnt).pos;
				p = camOldPos;
				p.y = 0;
				renderer->createVoxelModel(editEnt, false);
			}

			int spd = 64;
			if (platform::isKeyActive(platform::getKey("Left Shift"))) spd *= 8;
			int d = platform::isKeyActive(platform::getKey("up")) - platform::isKeyActive(platform::getKey("down"));
			transformStorage->getTransform(editEnt).pos.x += spd * dt * d;
			d = platform::isKeyActive(platform::getKey("right")) - platform::isKeyActive(platform::getKey("left"));
			transformStorage->getTransform(editEnt).pos.z += spd * dt * d;
			d = platform::isKeyActive(platform::getKey("PageUp")) - platform::isKeyActive(platform::getKey("PageDown"));
			transformStorage->getTransform(editEnt).pos.y += spd * dt * d;
			renderer->updateVoxelModel(editEnt);
		}


		if (platform::isMouseButtonDown(MouseButton::Left))
		{
			Vec3 tp = (ct.rotation * Vec3(0, 0, -20)) + ct.pos;
			if (collision->raycast(ct.pos, ct.rotation * Vec3(0, 0, -30), &tp))
			{
				log("%f %f %f", tp.x, tp.y, tp.z);
				int r = 6;
				auto ce = collision->getBoxIntetsectionEntities({tp, Vec3((float)r)});
				log("hits %d", ce.size());
				for (auto e : ce)
				{
					//destroyEntity();
					//continue;
					auto& m = *vmStorage->getModelForWrite(e);
					Vec3 off = transformStorage->getTransform(e).pos;
					auto p = m.destoyAndGetSphere(tp - off, r);
					renderer->updateVoxelModel(e);
				}
			}
		}
	}

	//{
	//Transform t;
	//VoxelModel* grass = getVoxelModel("grass");
	//for (int i = 0; i < 32; ++i)
	//	for (int j = 0; j < 32; ++j)
	//	{
	//		t.pos.x = i * 128.0f;
	//		t.pos.z = j * 128.0f;
	//		t.pos.y = -2;
	//		renderer->renderVoxelModel(grass, t);
	//	}

	//	t = Transform();
	//	//for (int i = 0; i < 16; ++i)
	//	//	for (int j = 0; j < 16; ++j)
	//	//	{
	//	//		//if (i != 0 && j !=0 ) continue;
	//	//		t.pos.x = i * 128.0f;
	//	//		t.pos.z = j * 128.0f;
	//	//		//t.rotation = Quaternion(Vec3(0.0f, frame * Deg2Rad,0.0f));

	//	//		renderer->renderVoxelModel(&voxelModels[(i + j) % (voxelModels.size() - 1)], t);
	//	//		//Renderer::renderVoxelModel(&voxelModels.front(), t);

	//	//	}
	//}
}

Array<Entity, 8092>& World::getCell(int x, int y)
{
	return grid[x + WorldCellsX*y];
}


Entity World::createEntity(const char* name)
{
	return ::createEntity(name);
}

void World::destroyEntity(Entity e)
{
	::destroyEntity(e);
}
