#pragma once 
#include "../common.h"
#include "../math.h"
#include "../Array.h"
#include "../Color.h"
#include "../VoxelData.h"

class SparseVoxelOctree
{
	struct Node
	{
		int firstChild;
		VoxelData voxel;

		Node();
	};

	int size = 0;
	Array<Node, 65536> nodes;

	VoxelData* insertVoxel(Vec3i pos, VoxelData voxel, int node, Vec3i boundsPos, int size);
	void eraseVoxel(Vec3i pos, int node, Vec3i boundPos, int size);
public:
	SparseVoxelOctree(int size);

	void insertVoxel(Vec3i pos, VoxelData voxel);
	void eraseVoxel(Vec3i pos);
};