#include "SparseVoxelOctree.h"

SparseVoxelOctree::Node::Node():
firstChild(-1)
{
}

SparseVoxelOctree::SparseVoxelOctree(Int size): size(size)
{
	nodes.emplace_back();
}

VoxelData* SparseVoxelOctree::insertVoxel(Vec3i p, VoxelData voxel, int nodeId, Vec3i bp, int size)
{
	if (p.x < bp.x || p.x >= bp.x + size || p.y < bp.y || p.y >= bp.y + size || p.z < bp.z || p.z >= bp.z + size)
		return 0;

	Node& node = this->nodes[nodeId];
	if (size == 1)
	{
		node.voxel = voxel;
		return &node.voxel;
	}

	if (node.firstChild == 0)
	{
		node.firstChild = (int)this->nodes.size();
		this->nodes.resize(node.firstChild + 8);
	}

	int hs = size / 2;

	VoxelData* ptr;
	ptr = insertVoxel(p, voxel, nodeId, bp, hs);
	if (ptr != nullptr) return ptr;
	ptr = insertVoxel(p, voxel, nodeId, bp + Vec3i(hs, 0, 0), hs);
	if (ptr != nullptr) return ptr;
	ptr = insertVoxel(p, voxel, nodeId, bp + Vec3i(0, hs, 0), hs);
	if (ptr != nullptr) return ptr;
	ptr = insertVoxel(p, voxel, nodeId, bp + Vec3i(hs, hs, 0), hs);
	if (ptr != nullptr) return ptr;
	ptr = insertVoxel(p, voxel, nodeId, bp + Vec3i(0, 0, hs), hs);
	if (ptr != nullptr) return ptr;
	ptr = insertVoxel(p, voxel, nodeId, bp + Vec3i(hs, 0, hs), hs);
	if (ptr != nullptr) return ptr;
	ptr = insertVoxel(p, voxel, nodeId, bp + Vec3i(0, hs, hs), hs);
	if (ptr != nullptr) return ptr;
	return ptr = insertVoxel(p, voxel, nodeId, bp + Vec3i(hs, hs, hs), hs);
}

void SparseVoxelOctree::eraseVoxel(Vec3i p, int node, Vec3i bp, int size)
{
	if (p.x < bp.x || p.x >= bp.x + size || p.y < bp.y || p.y >= bp.y + size || p.z < bp.z || p.z >= bp.z + size)
		return;
}

void SparseVoxelOctree::insertVoxel(Vec3i pos, VoxelData voxel)
{
	insertVoxel(pos, voxel, 0, Vec3i(0, 0, 0), size);
}

void SparseVoxelOctree::eraseVoxel(Vec3i pos)
{
	eraseVoxel(pos, 0, Vec3i(0, 0, 0), size);
}