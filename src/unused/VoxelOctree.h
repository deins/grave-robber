#pragma once
#include "common.h"
#include "math.h"
#include "Array.h"
#include "Color.h"

template <int Size = 4096, int LeafSize = 32> // Size and Leaf size = cube edge length in voxels
class VoxelWorldChunk
{
	const int Levels = log2(Size) - log2(LeafSize); // excluding leafs
	const int NodeCount = pow(8, Levels - 1);
	typedef int NodeIdx;

	struct Node
	{
		Color color;
	};

	Array<Node, NodeCount> nodes;
	NodeIdx getClild(NodeIdx node, int childrenIdx) const;
	NodeIdx getParent(NodeIdx node) const;
public:

};

template <int Size, int LeafSize>
NodeIdx VoxelWorldChunk::getClild(NodeIdx node, int childrenIdx)
{
	return node * 8 + 1;
}

template <int Size, int LeafSize>
NodeIdx VoxelWorldChunk::getParent(NodeIdx node)
{
	return (node - 1) / 8;
}
