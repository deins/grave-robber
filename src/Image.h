#pragma once
#include "Color.h"
#include "common.h"

class Image : NonCopyable
{
	unsigned char* pixels = nullptr;
	int width = 0, height = 0, components = 0;
public:
	Image() = default;
	~Image();
	void loadFromFile(const char* filename, int requiredComponents = 0);
	int getWidth() const;
	int getHeight() const;
	int getComponents() const;
	Color getPixel(int x, int y) const;
	void setPixel(int x, int y, Color c) ;
	unsigned char* data();
};