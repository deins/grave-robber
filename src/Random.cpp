#include "Random.h"
#include "debug.h"
#include <string.h>

Random::Random() 
{
	// just some randomization actions
	state[*reinterpret_cast<UInt32*>(this) % 16] = (*reinterpret_cast<UInt32*>(this));
	state[*reinterpret_cast<UInt32*>(this) % 16] = *reinterpret_cast<UInt32*>(this) * 132;
	state[*reinterpret_cast<UInt32*>(this) * 456 % 16] = *reinterpret_cast<UInt32*>(this) ^ 0xA56A88D2;
}

UInt32 Random::nextUInt32()
{
	UInt32 a, b, c, d;
	a = state[index];
	c = state[(index + 13) & 15];
	b = a^c ^ (a << 16) ^ (c << 15);
	c = state[(index + 9) & 15];
	c ^= (c >> 11);
	a = state[index] = b^c;
	d = a ^ ((a << 5) & 0xDA442D24UL);

	index = (index + 15) & 15;
	a = state[index];
	state[index] = a^b^d ^ (a << 2) ^ (b << 18) ^ (c << 28);
	return state[index];
}

UInt64 Random::nextUInt64()
{
	return (UInt64(nextUInt32()) << 32) | nextUInt32();
}

// in range [a;b)
Int32 Random::inRange(Int32 a, Int32 b)
{
	assert(a < b);
	UInt32 r = nextUInt32();
	r %= b - a ;
	return a + int(r);
}