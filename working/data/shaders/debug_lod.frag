#version 150
#ifdef GL_ES 
	// Android GSLS
	precision lowp float;
#else
	// PC GSLS
	#define lowp
	#define mediump
	#define highp
#endif

uniform float Size;
//uniform sampler2D tex;
uniform vec4 Color;
uniform float Frame;

in vec4 fragColor; 

void main()
{
	vec4 colors[] = vec4[](vec4(1),vec4(1,0,0,1),vec4(0,1,0,1),vec4(0,0,1,1),vec4(1,1,0,1),vec4(0,1,1,1));
	vec4 c = fragColor * colors[int(log2(Size))];
	gl_FragColor = c;
}