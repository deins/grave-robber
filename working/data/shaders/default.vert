#version 150

uniform mat4 MVP,Model, View, Projection;
uniform vec3 CamPos;
in vec4 Position;
in vec4 Color;

out vec4 vertexColor;

void main()
{
	vertexColor = Color;
	gl_Position =  Position;
}

