#version 150

uniform mat4 Model, View;
uniform vec3 CamPos;
in vec4 Position;
in vec4 Color;

out vec4 VertexColor;

void main()
{
	VertexColor = Color ;//* 64.0/distance(CamPos,(Model * Position).xyz); // lighting
	gl_Position =  View * Model * Position;
}

