#version 150
layout(points) in;
layout(triangle_strip, max_vertices=24) out;
uniform mat4 MVP,Model, View, Projection;
uniform float Frame;
uniform float Size;
in vec4 vertexColor[];
out vec4 fragColor;

void main()
{
	float s = Size *.5;
	vec4 cubeverts[] = vec4 [](
		vec4(s,-s, s,1), vec4(s,s, s,1), vec4(-s,-s, s,1), vec4(-s,s, s,1),
		vec4(s,-s, -s,1), vec4(s,s, -s,1), vec4(-s,-s, -s,1), vec4(-s,s, -s,1)
	);
	for (int i=0; i<8; ++i) {
		cubeverts[i] = cubeverts[i] + gl_in[0].gl_Position;
		cubeverts[i].w = 1;
		cubeverts[i] = MVP* cubeverts[i];
	}

	const int faces[] = int[](
		0,1,2,3, // front
		4,6,5,7, // back
		0,4,1,5, // left
		3,7,2,6, // right
		1,5,3,7, // top	
		4,0,6,2  // bottom
	);
	//const vec4 colors[] = vec4[](
	//	vec4(1,0,0,1), vec4(0,1,0,1),vec4(0,0,1,1),
	//	vec4(1,1,0,1),vec4(0,1,1,1),vec4(1,1,1,1));
	//const vec4 colors[] = vec4[](
	//	vec4(0,0,0,1), vec4(vec3(.1f),1),vec4(vec3(.05f),1),
	//	vec4(vec3(.15f),1),vec4(vec3(-.1f),1),vec4(vec3(-.05f),1));

	for (int i=0; i<6; ++i){
		vec4 col = vertexColor[0] ;
		gl_Position = cubeverts[faces[i*4]];
		fragColor = col ;
		EmitVertex();
		gl_Position = cubeverts[faces[i*4+1]];
		fragColor = col;
		EmitVertex();
		gl_Position = cubeverts[faces[i*4+2]];
		fragColor = col ;
		EmitVertex();
		gl_Position = cubeverts[faces[i*4+3]];
		fragColor = col;
		EmitVertex();
		EndPrimitive();
	}
//	drawWorldGizmo();
}  