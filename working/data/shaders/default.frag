#version 150
#ifdef GL_ES 
	// Android GSLS
	precision lowp float;
#else
	// PC GSLS
	#define lowp
	#define mediump
	#define highp
#endif

//uniform sampler2D tex;
uniform vec4 Color;
uniform float Frame;

in vec4 fragColor; 

void main()
{
	gl_FragColor = fragColor;
}