#version 150
layout(points) in;
layout(triangle_strip, max_vertices=4) out;
uniform mat4 MVP,Model, View, Projection;
uniform vec3 CamPos;
uniform float Frame;
uniform float Size;
in vec4 VertexColor[];
out vec4 fragColor;
void main()
{
    vec4 col = VertexColor[0] ;
    vec4 P = gl_in[0].gl_Position;
    // a: left-bottom 
  vec2 va = P.xy + vec2(-0.5, -0.5) * Size;
    gl_Position = Projection * vec4(va, P.zw);
    fragColor = col ;
    EmitVertex();
    // d: right-bottom
  vec2 vd = P.xy + vec2(0.5, -0.5) * Size;
    gl_Position = Projection * vec4(vd, P.zw);
    fragColor = col ;
    EmitVertex();
    // b: left-top
  vec2 vb = P.xy + vec2(-0.5, 0.5) * Size;
    gl_Position = Projection * vec4(vb, P.zw);
    fragColor = col ;
    EmitVertex();
    // c: right-top
  vec2 vc = P.xy + vec2(0.5, 0.5) * Size;
    gl_Position = Projection * vec4(vc, P.zw);
    fragColor = col ;
    EmitVertex();
    EndPrimitive();
}

